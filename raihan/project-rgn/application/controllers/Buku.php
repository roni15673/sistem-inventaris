<?php

class Buku extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_buku');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Buku';
        $data['buku'] = $this->m_buku->getAllbuku();
        $this->load->view('templates/header', $data);
        $this->load->view('buku/v_buku');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('pengarang', 'Pengarang', 'required');
        $this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
        $this->form_validation->set_rules('tahun_terbit', 'tahun_terbit', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('buku/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_buku->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('buku');
        }
       
    }

    public function hapus($id){

        $this->m_buku->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('buku');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['buku'] = $this->m_buku->getAllbukubyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('buku/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['buku'] = $this->m_buku->getAllbukubyid($id);

        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('pengarang', 'Pengarang', 'required');
        $this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
        $this->form_validation->set_rules('tahun_terbit', 'Tahun_terbit', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('buku/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_buku->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('buku');
        }
       
    }

}
?>