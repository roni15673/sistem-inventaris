<?php

class Jurusan extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_jurusan');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'jurusan';
        $data['jurusan'] = $this->m_jurusan->getAlljurusan();
        $this->load->view('templates/header', $data);
        $this->load->view('jurusan/v_jurusan');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';

       
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('jurusan/v_tambah');
            $this->load->view('templates/footer');
        } else {
            $this->m_jurusan->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('jurusan');
        }
       
    }

    public function hapus($id){

        $this->m_jurusan->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('jurusan');

    }

    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['jurusan'] = $this->m_jurusan->getAlljurusanbyid($id);

       
        $this->form_validation->set_rules('nama', 'Nama', 'required');
    
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('jurusan/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_jurusan->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('jurusan');
        }
       
    }

}
?>