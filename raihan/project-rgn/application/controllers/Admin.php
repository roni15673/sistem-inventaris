<?php 
 
class Admin extends CI_Controller{
 
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}
 
	function index(){

		$data['judul'] = 'administrator';
		$this->load->view('admin/header_admin',  $data);
		$this->load->view('admin/v_admin');
		$this->load->view('templates/footer');
	}
}