<?php

class Mahasiswa extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_mahasiswa');
        $this->load->model('m_jurusan');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'mahasiswa';
        $data['mahasiswa'] = $this->m_mahasiswa->getAllmahasiswa();
        $this->load->view('templates/header', $data);
        $this->load->view('mahasiswa/v_mahasiswa');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
        $data['jurusan'] = $this->m_jurusan->getAlljurusan();

        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('nrp', 'Nrp', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('mahasiswa/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_mahasiswa->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('mahasiswa');
        }
       
    }

    public function hapus($id){

        $this->m_mahasiswa->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('mahasiswa');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['mahasiswa'] = $this->m_mahasiswa->getAllmahasiswabyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('mahasiswa/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['mahasiswa'] = $this->m_mahasiswa->getAllmahasiswabyid($id);
        $data['jurusan'] = $this->m_jurusan->getAlljurusan();
        
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('nrp', 'Nrp', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('mahasiswa/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_mahasiswa->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('mahasiswa');
        }
       
    }

}
?>