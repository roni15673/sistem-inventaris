<?php

class M_mahasiswa extends CI_model{

    public function getAllmahasiswa(){
         return $this->db->get('mahasiswa')->result_array();
}

    public function tambah_data(){
        $data = [
            "id" => $this->input->post('id', true),
            "nama" => $this->input->post('nama', true),
            "nrp" => $this->input->post('nrp', true),
            "email" => $this->input->post('email', true),
            "jurusan" => $this->input->post('jurusan', true)

        ];

        $this->db->insert('mahasiswa', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('mahasiswa', ['id' => $id]);
    }

    public function getAllmahasiswabyid($id){

       return $this->db->get_where('mahasiswa',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "id" => $this->input->post('id', true),
            "nama" => $this->input->post('nama', true),
            "nrp" => $this->input->post('nrp', true),
            "email" => $this->input->post('email', true),
            "jurusan" => $this->input->post('jurusan', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('mahasiswa', $data);
    }

    

}
?>