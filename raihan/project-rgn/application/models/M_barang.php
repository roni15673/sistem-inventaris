<?php

class M_barang extends CI_model{

    public function getAllbarang(){
         return $this->db->get('barang')->result_array();
}

    public function tambah_data(){
        $data = [
            
            "nama_barang" => $this->input->post('nama_barang', true),
            "harga_satuan" => $this->input->post('harga_satuan', true),
            "jumlah_barang" => $this->input->post('jumlah_barang', true)

        ];

        $this->db->insert('barang', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('barang', ['id_barang' => $id]);
    }

    public function getAllbarangbyid($id){

       return $this->db->get_where('barang',['id_barang' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            
            "nama_barang" => $this->input->post('nama_barang', true),
            "harga_satuan" => $this->input->post('harga_satuan', true),
            "jumlah_barang" => $this->input->post('jumlah_barang', true)

        ];

        $this->db->where('id_barang', $this->input->post('id_barang'));
        $this->db->update('barang', $data);
    }

    

}
