<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  </head>
  <body>
  <?php
  ?>
    <div class="container">
     <?php if($this->session->flashdata('flash') ) :?>
    <div class="row mt-3">
        <div class="col-md-6">
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
            Daftar Barang<strong> berhasil!</strong> <?= $this->session->flashdata('flash'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
    </div>
<?php endif;?>
    
    <div class="row mt-3">
      <div class="col-md-6">
      <?php 
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/barang/tambah" class="href btn btn-primary">Tambah Data</a>
         <?php } ?>
      </div>
    </div>
    
    
        <div class="row mt-3">
            <div class="col-md-6">
            <h3>Daftar Barang</h3>
  <table class="table table-bordered">
    <?php 
            $no =1;
            foreach($barang as $u) : ?>
  <tbody>
    <tr>
      <td><?php print $no++ ?></td>
      <td><?php echo $u['nama_barang'];?></td>
      <td><img src="<?php echo base_url();?>asset/img/default.jpg" alt="..." class="img-thumbnail" width= 70px></td>
      <td> <a href="<?php echo base_url();?>index.php/barang/detail/<?= $u ['id_barang'];?>" class="badge badge-primary float-left">Detail</a>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/barang/edit/<?= $u ['id_barang'];?>" class="badge badge-success float-left">Edit</a>
      <a href="<?php echo base_url();?>index.php/barang/hapus/<?= $u ['id_barang'];?>" class="badge badge-danger float-left">Hapus</a>
     <?php } ?></td>
    </tr>
  </tbody>
    <?php endforeach;?>
</table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>