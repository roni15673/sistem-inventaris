<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">

    <title></title>
  </head>
  <body>
  <div class="container">
    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                 <div class="card-header">
                         Form Edit
                </div>
            <div class="card-body">
                  <form action="" method="post">
                  <input type="hidden" name="id_barang" value="<?php echo $user['id']; ?>">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $user['nama']; ?>">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $user['alamat']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('alamat'); ?></small>
                </div>
                <div class="form-group">
                    <label for="agama">Agama</label>
                    <input type="text" class="form-control" id="agama" name="agama" value="<?php echo $user['agama']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('agama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $user['tempat_lahir']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('tempat lahir'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="<?php echo $user['tanggal_lahir']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('tanggal lahir'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?php echo $user['jenis_kelamin']; ?>">
                      <option id="Laki-laki" value="Laki-laki">Laki-laki</option>
                      <option  id="jenis_kelamin" value="Perempuan">Perempuan</option>
                    </select>
                </div>
                 <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $user['username']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('username'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="<?php echo $user['password']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('password'); ?></small>
                </div>
 <input type="hidden" name="level" value="user">
                     <button type="submit" name="edit" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
            </div>
        </div>

          
        </div>
    </div>
  </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>