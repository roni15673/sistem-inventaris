<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">

    <title></title>
  </head>
  <body>
  <div class="container">
    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                 <div class="card-header">
                         Form Tambah
                </div>
            <div class="card-body">
                  <form action="" method="post">
                <div class="form-group">
                    <label for="id">id</label>
                    <input type="number" class="form-control" id="id" name="id">
                    <small class="form-text text-danger"><?php echo form_error('id'); ?></small>
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                     <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="nrp">Nrp</label>
                    <input type="number" class="form-control" id="nrp" name="nrp">
                     <small class="form-text text-danger"><?php echo form_error('nrp'); ?></small>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email">
                     <small class="form-text text-danger"><?php echo form_error('email'); ?></small>
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                         <select class="form-control" id="jurusan" name="jurusan">
                            <?php foreach ($jurusan as $j) : ?>
					<option value="<?php $j['id']; ?>"><?php echo $j['nama']; ?></option>
				<?php endforeach; ?>
                        </select>
                    </div>
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
            </div>
        </div>

          
        </div>
    </div>
  </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>