<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  </head>
  <body>
  <?php
  ?>
    <div class="container">
     <?php if($this->session->flashdata('flash') ) :?>
    <div class="row mt-3">
        <div class="col-md-6">
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
            Data buku<strong> berhasil!</strong> <?= $this->session->flashdata('flash'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
    </div>
<?php endif;?>
    
    <div class="row mt-3">
      <div class="col-md-6">
       <?php 
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/buku/tambah" class="href btn btn-primary">Tambah Data</a>
         <?php } ?>
      </div>
    </div>
    
    
        <div class="row mt-3">
            <div class="col-md-6">
            <h3>Daftar Buku</h3>
            <ul class="list-group">
            <?php foreach($buku as $u) : ?>
                <li class="list-group-item"><?php echo $u['judul'];?>
                  <a href="<?php echo base_url();?>index.php/buku/detail/<?= $u ['id'];?>" class="badge badge-primary float-right">Detail</a>
                   <?php 
                    if($this->session->userdata('permission') =='admin') { ?>

                   <a href="<?php echo base_url();?>index.php/buku/edit/<?= $u ['id'];?>" class="badge badge-success float-right">Edit
                   </a>
                   <a href="<?php echo base_url();?>index.php/buku/hapus/<?= $u ['id'];?>" class="badge badge-danger float-right" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>
                   <?php } ?>
                </li>
            <?php endforeach;?>
            </ul>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>