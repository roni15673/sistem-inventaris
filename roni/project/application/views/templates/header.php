<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/style.css">
     <link rel="stylesheet" href="<?php echo base_url();?>asset/fontawesome/css/font-awesome.min.css" />
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">


    <title><?php echo $judul;?></title>
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">
  </head>
  <body>

  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark ">
  <a class="navbar-brand" href="<?php echo base_url();?>index.php/home"><img src="<?php echo base_url();?>asset/img/logo.png" alt="..." width= 40px height=40px></td> <b>Sistem Inventaris</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<div class="container ">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/home"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/barang"><i class="fa fa-balance-scale "></i>

Daftar Barang</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/user"><i class="fa fa-users"></i> Daftar User</a></li>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#"><i class="fa fa-ellipsis-v "></i></a>
      </li>
      
   
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-left"></i></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href=""><!-- <i class="fa fa-user"></i> -->Sistem Inventaris</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo site_url();?>/inventaris/"><!-- <i class="fa fa-wrench "></i> -->Inventaris</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/letak/"><!-- i class="fa fa-wrench "></i> -->Letak</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/merek/"><!-- <i class="fa fa-wrench "></i> -->Merek</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/status/"><!-- i class="fa fa-wrench "></i> -->Status</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/satuan"><!-- <i class="fa fa-wrench "></i> -->Satuan</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/ukuran/"><!-- <i class="fa fa-wrench "></i> -->Ukuran</a>
        </div>
      </li>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-success my-1 my-sm-0 " type="submit"><img src="<?php echo base_url();?>asset/img/search.png" alt="..." width= 20px height=20px margin-top:10px></button>
          <?php 
        if($this->session->userdata('permission') ==false ) { ?>
          <div class="loginlogout">
        <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/login"><i class="fa fa-key"> Login</i></a></li> 
         <?php } ?>
          <div class="loginlogout">
         <?php 
        if($this->session->userdata('permission') ==true ) { ?>
        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('login/logout'); ?>"><i class="fa fa-undo"> Logout</i></a></li>
      </div>
        </div>
         <?php } ?>
        </ul>
    </div>
    </form>
  </div>
</nav>

<!-- end navbar -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>