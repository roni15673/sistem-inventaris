<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/style.css">

  </head>
  <body>
  <?php
  ?>
    <div class="container">
     <?php if($this->session->flashdata('flash') ) :?>
    <div class="row mt-3">
        <div class="col-md-6">
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
            Daftar Barang<strong> berhasil!</strong> <?= $this->session->flashdata('flash'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
    </div>
<?php endif;?>
    
    <div class="row mt-3">
      <div class="col-md-6">
      <?php 
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/barang/tambah" class="href btn btn-primary">Tambah Data</a>
         <?php } ?>
      </div>
    </div>
    
    
        <div class="row mt-3">
            <div class="col-md-6">
            <h3>Daftar Barang</h3>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Barang</th>
      <th scope="col">Gambar</th>
      <th scope="col">Inventaris</th>
      <th scope="col">Letak</th>
      <th scope="col">Merek</th>
      <th scope="col">Asal</th>
      <th scope="col">Status</th>
      <th scope="col">Bahan</th>
      <th scope="col">Ukuran</th>
      <th scope="col">Tahun</th>
      <th scope="col">Jumlah</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
            $no =1;
            foreach($barang as $u) : ?>
    <tr>
      <th scope="row"><?php print $no++ ?></th>
      <td><?php echo $u['nama'];?></td>
      <td><img src="<?php echo base_url();?>asset/img/default.jpg" alt="..." class="img-thumbnail" width= 70px></td>
      <td><?php echo $u['inventaris'];?></td>
      <td><?php echo $u['letak'];?></td>
      <td><?php echo $u['merek'];?></td>
      <td><?php echo $u['asal'];?></td>
      <td><?php echo $u['status'];?></td>
      <td><?php echo $u['bahan'];?></td>
      <td><?php echo $u['ukuran'];?></td>
      <td><?php echo $u['tahun'];?></td>
      <td><?php echo $u['jumlah'];?></td>
       <td> <a href="<?php echo base_url();?>index.php/barang/detail/<?= $u ['id'];?>" class="badge badge-primary float-left"><i class="fa fa-file"> Detail</i></a>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/barang/edit/<?= $u ['id'];?>" class="badge badge-success float-left"><i class="fa fa-edit"> Edit</i></a>
      <a href="<?php echo base_url();?>index.php/barang/hapus/<?= $u ['id'];?>" class="badge badge-danger float-left"><i class="fa fa-trash"> Hapus</i></a>
     <?php } ?></td>
    </tr>
     <?php endforeach;?>
  </tbody>
  
</table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>