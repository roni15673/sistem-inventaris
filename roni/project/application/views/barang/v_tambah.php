<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">

    <title></title>
  </head>
  <body>
  <div class="container">
    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                 <div class="card-header">
                         Form Tambah
                </div>
            <div class="card-body">
                  <form action="" method="POST">
                <div class="form-group">
                    <label for="nama">Nama Barang</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="inventaris">Inventaris<?php echo form_error('inventaris'); ?></label>
                    <select class="form-control" id="inventaris" name="inventaris">
                          <option disabled="" selected="">Pilih Inventaris</option>
                              <?php
                              foreach ($inventaris as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['jenis']; ?>"><?php echo $value['jenis']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                  <div class="form-group">
                    <label for="letak">Letak<?php echo form_error('letak'); ?></label>
                    <select class="form-control" id="letak" name="letak">
                          <option disabled="" selected="">Pilih Letak</option>
                               <?php foreach ($letak as $key => $value ) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="merek">Merek<?php echo form_error('merek'); ?></label>
                    <select class="form-control" id="merek" name="merek">
                          <option disabled="" selected="">Pilih Merek</option>
                               <?php foreach ($merek as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="asal">Asal</label>
                    <input type="text" class="form-control" id="asal" name="asal">
                     <small class="form-text text-danger"><?php echo form_error('asal'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="status">Status<?php echo form_error('status'); ?></label>
                    <select class="form-control" id="status" name="status">
                          <option disabled="" selected="">Pilih Status</option>
                               <?php foreach ($status as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="bahan">Bahan</label>
                    <input type="text" class="form-control" id="bahan" name="bahan">
                     <small class="form-text text-danger"><?php echo form_error('bahan'); ?></small>
                </div>
                <div class="form-group">
                    <label for="satuan">Satuan<?php echo form_error('satuan'); ?></label>
                    <select class="form-control" id="satuan" name="satuan">
                          <option disabled="" selected="">Pilih Satuan</option>
                               <?php foreach ($satuan as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="ukuran">Ukuran<?php echo form_error('ukuran'); ?></label>
                    <select class="form-control" id="ukuran" name="ukuran">
                          <option disabled="" selected="">Pilih Ukuran</option>
                               <?php foreach ($ukuran as $key => $value) : ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" id="tahun" name="tahun" min="2000" max="2019" >
                     <small class="form-text text-danger"><?php echo form_error('tahun'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" class="form-control" id="jumlah" name="jumlah" min="0" >
                     <small class="form-text text-danger"><?php echo form_error('jumlah'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" >
                     <small class="form-text text-danger"><?php echo form_error('harga'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="ket">Keterangan</label>
                    <input type="text" class="form-control" id="ket" name="ket">
                     <small class="form-text text-danger"><?php echo form_error('keterangan'); ?></small>
                </div>
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/barang" class="btn btn-primary float-right"><i class="fa fa-arrow-circle-left  ">Back</i></a>
            </div>
        </div>

          
        </div>
    </div>
  </div>



  <!--   <script type="text/javascript">
    
    var rupiah = document.getElementById('harga_satuan');
    rupiah.addEventListener('keyup', function(e){
      // tambahkan 'Rp.' pada saat form di ketik
      // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
      rupiah.value = formatRupiah(this.value, 'Rp. ');
    });
 
    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split       = number_string.split(','),
      sisa        = split[0].length % 3,
      rupiah        = split[0].substr(0, sisa),
      ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
 
      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }
 
      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  </script> -->
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>