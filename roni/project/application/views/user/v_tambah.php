<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

    <title></title>
  </head>
  <body>
  <div class="container">
    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                 <div class="card-header">
                         Form Register
                </div>
            <div class="card-body">
                  <form action="" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat">
                     <small class="form-text text-danger"><?php echo form_error('alamat'); ?></small>
                </div>
                <div class="form-group">
                    <label for="agama">Agama</label>
                    <select class="form-control" id="agama" name="agama">
                      <option disabled="" selected="">Pilih Agama</option>
                      <option id="agama" value="Islam">Islam</option>
                       <option id="agama" value="Kristen Protestan">Kristen Protestan</option>
                        <option id="agama" value="katolik">katolik</option>
                         <option id="agama" value="Hindu">Hindu</option>
                          <option id="agama" value="Buddha">Buddha</option>
                           <option id="agama" value="Kong Hu Cu">Kong Hu Cu</option>
                    </select>
                     <small class="form-text text-danger"><?php echo form_error('agama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="year" class="form-control" id="tempat_lahir" name="tempat_lahir">
                     <small class="form-text text-danger"><?php echo form_error('tempat lahir'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir">
                     <small class="form-text text-danger"><?php echo form_error('tanggal lahir'); ?></small>
                </div>
                 <div class="form-group">

                     <small class="form-text text-danger"><?php echo form_error('jenis kelamin'); ?></small>
                    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                      <option disabled="" selected="">Pilih Jenis Kelamin</option>
                       <option id="jenis_kelamin" value="Laki-laki">Laki-laki</option>
                        <option  id="jenis_kelamin" value="Perempuan">Perempuan</option>
                    </select>

                </div>
                 <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                     <small class="form-text text-danger"><?php echo form_error('username'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                     <small class="form-text text-danger"><?php echo form_error('password'); ?></small>
                </div>
                  <input type="hidden" name="level" value="user">
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
            <a href="<?php echo site_url(); ?>/login" class="btn btn-primary float-right"><i class="fa fa-arrow-circle-left  ">Back</i></a>
            </div>
        </div>

          
        </div>
    </div>
  </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>