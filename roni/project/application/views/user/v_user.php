<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  </head>
  <body>
  <?php
  ?>
    <div class="container">
     <?php if($this->session->flashdata('flash') ) :?>
    <div class="row mt-3">
        <div class="col-md-6">
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
            Data <strong> berhasil!</strong> <?= $this->session->flashdata('flash'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
    </div>
<?php endif;?>
    
    <div class="row mt-3">
      <div class="col-md-6">
      </div>
    </div>
        <div class="row mt-3">
            <div class="col-md-6">
            <h3>Daftar User</h3>
            
    <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Foto</th>
      <th scope="col">Alamat</th>
      <th scope="col">Agama</th>
      <th scope="col">Tempat Lahir</th>
      <th scope="col">Tanggal Lahir</th>
      <th scope="col">Jenis Kelamin</th>
      <th scope="col">Username</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
            $no =1;
            foreach($user as $u) : ?>
    <tr>
      <th scope="row"><?php print $no++ ?></th>
      <td><?php echo $u['nama'];?></td>
      <td><img src="<?php echo base_url();?>asset/img/default.jpg" alt="..." class="img-thumbnail" width= 70px></td>
      <td><?php echo $u['alamat'];?></td>
      <td><?php echo $u['agama'];?></td>
      <td><?php echo $u['tempat_lahir'];?></td>
      <td><?php echo $u['tanggal_lahir'];?></td>
      <td><?php echo $u['jenis_kelamin'];?></td>
      <td><?php echo $u['username'];?></td>
       <td> <a href="<?php echo base_url();?>index.php/user/detail/<?= $u ['id'];?>" class="badge badge-primary float-left">Detail</a>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/user/edit/<?= $u ['id'];?>" class="badge badge-success float-left">Edit</a>
      <a href="<?php echo base_url();?>index.php/user/hapus/<?= $u ['id'];?>" class="badge badge-danger float-left">Hapus</a>
     <?php } ?></td>
    </tr>
     <?php endforeach;?>
  </tbody>
  
</table>
</div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>