<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">

    <title>Home</title>
  </head>
  <body>
  <?php
  ?>
    <div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Detail Data User
                </div>
            <div class="card-body">
              <img src="<?php echo base_url();?>asset/img/default.jpg" alt="..." class="img-thumbnail" width=150px>
               <h5 class="card-title"><?php echo $user['nama'];?></h5>
                 Id : <h6 class="card-subtitle mb-2 text-muted"><?php echo $user['id'];?></h6>
               Alamat : <p class="card-text"><?php echo $user['alamat'];?></p>
               Agama : <p class="card-text"><?php echo $user['agama'];?></p>
               Tempat Lahir : <p class="card-text"><?php echo $user['tempat_lahir'];?></p>
               Tanggal Lahir : <p class="card-text"><?php echo $user['tanggal_lahir'];?></p>
               Jenis Kelamin : <p class="card-text"><?php echo $user['jenis_kelamin'];?></p>
               Username : <p class="card-text"><?php echo $user['username'];?></p>
                <a href="<?php echo site_url(); ?>/user" class="btn btn-primary"><i class="fa fa-arrow-circle-left  ">Back</i></a>
            </div>
</div>
        </div>
    </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</hUser