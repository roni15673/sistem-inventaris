<?php

class Inventaris extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_inventaris');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Inventaris';
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $this->load->view('templates/header', $data);
        $this->load->view('inventaris/v_inventaris');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('jenis', 'Jenis', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('inventaris/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_inventaris->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('inventaris');
        }
       
    }

    public function hapus($id){

        $this->m_inventaris->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('inventaris');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['inventaris'] = $this->m_inventaris->getAllinventarisbyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('inventaris/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['inventaris'] = $this->m_inventaris->getAllinventarisbyid($id);

        $this->form_validation->set_rules('jenis', 'Jenis', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('inventaris/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_inventaris->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('inventaris');
        }
       
    }

}
?>