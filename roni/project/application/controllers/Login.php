<?php 
 
class Login extends CI_Controller{
 
   public function __construct(){
        parent::__construct();      
        $this->load->model('m_login');
        $this->load->model('m_user');
 
    }
 
   public function index(){

        $data['judul'] = 'halaman login';
        // $this->load->view('templates/header', $data);
        $this->load->view('login/v_login', $data);
         // $this->load->view('templates/footer');
    }
 
    public function aksi_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => $password
            );
        $cek = $this->m_login->cek_login("user",$where)->num_rows();
        
        if($cek > 0){
            $data = $this->m_login->cek_login("user",$where)->result_array();
            $data_session = array(
                'nama' => $username,
                'permission' => $data[0]['level'],
                'status' => "login"
                );
 
            $this->session->set_userdata($data_session);
                 redirect(site_url('home'));
 
        }else{
            echo "Username dan password salah !";
            
        }
    }
 
    public function logout(){
        session_destroy();
        redirect(site_url('login'));
    }
}