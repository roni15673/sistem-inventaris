<?php

class Ukuran extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_ukuran');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Ukuran';
        $data['ukuran'] = $this->m_ukuran->getAllukuran();
        $this->load->view('templates/header', $data);
        $this->load->view('ukuran/v_ukuran');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('ukuran/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_ukuran->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('ukuran');
        }
       
    }

    public function hapus($id){

        $this->m_ukuran->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('ukuran');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['ukuran'] = $this->m_ukuran->getAllukuranbyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('ukuran/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['ukuran'] = $this->m_ukuran->getAllukuranbyid($id);

       $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('ukuran/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_ukuran->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('ukuran');
        }
       
    }

}
?>