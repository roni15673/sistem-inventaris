<?php

class Barang extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_barang');
        $this->load->model('m_inventaris');
        $this->load->model('m_letak');
        $this->load->model('m_merek');
        $this->load->model('m_status');
        $this->load->model('m_satuan');
        $this->load->model('m_ukuran');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Daftar Barang';
        $data['barang'] = $this->m_barang->getAllbarang();
        $this->load->view('templates/header', $data);
        $this->load->view('barang/v_barang');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['letak'] = $this->m_letak->getAllletak();
        $data['merek'] = $this->m_merek->getAllmerek();
        $data['status'] = $this->m_status->getAllstatus();
        $data['satuan'] = $this->m_satuan->getAllsatuan();
        $data['ukuran'] = $this->m_ukuran->getAllukuran();
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        // $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        // $this->form_validation->set_rules('letak', 'Letak', 'required');
        // $this->form_validation->set_rules('merek', 'Merek', 'required');
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        // $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('keadaan', 'Keadaan', 'required');
        $this->form_validation->set_rules('bahan', 'Bahan', 'required');
        // $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        // $this->form_validation->set_rules('ukuran', 'Ukuran', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required');
      
        if( $this->form_validation->run() == FALSE ) {
            // print_r($data['inventaris']);exit;
            // foreach ($data['inventaris'] as $key => $value) {
            //     print_r($value['jenis']);
            // }
            // exit;
            $this->load->view('templates/header', $data);
            $this->load->view('barang/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            // print_r($data);exit;
            // print_r($_POST);exit;
            $this->m_barang->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('barang');
        }
       
    }

    public function hapus($id){

        $this->m_barang->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('barang');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['barang'] = $this->m_barang->getAllbarangbyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('barang/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['barang'] = $this->m_barang->getAllbarangbyid($id);
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['letak'] = $this->m_letak->getAllletak();
        $data['merek'] = $this->m_merek->getAllmerek();
        $data['status'] = $this->m_status->getAllstatus();
        $data['satuan'] = $this->m_satuan->getAllsatuan();
        $data['ukuran'] = $this->m_ukuran->getAllukuran();

       $this->form_validation->set_rules('nama', 'Nama', 'required');
        // $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('merek', 'Merek', 'required');
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('bahan', 'Bahan', 'required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        $this->form_validation->set_rules('ukuran', 'Ukuran', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('barang/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_barang->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('barang');
        }
       
    }

}
?>