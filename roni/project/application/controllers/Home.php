<?php

class Home extends CI_Controller{

    public function __construct(){
        parent::__construct();
         $this->load->model('m_login');
    }


    public function index($nama = '')
    {

        $data['judul'] = 'Home';
        $data['nama'] = $nama;
         $this->load->view('templates/header', $data);
         $this->load->view('home/v_home',$data);
         $this->load->view('templates/footer');
    }


}

?>