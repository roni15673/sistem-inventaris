<?php

class Satuan extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_satuan');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Satuan';
        $data['satuan'] = $this->m_satuan->getAllsatuan();
        $this->load->view('templates/header', $data);
        $this->load->view('satuan/v_satuan');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('satuan/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_satuan->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('satuan');
        }
       
    }

    public function hapus($id){

        $this->m_satuan->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('satuan');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['satuan'] = $this->m_satuan->getAllsatuanbyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('satuan/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['satuan'] = $this->m_satuan->getAllsatuanbyid($id);

       $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('satuan/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_satuan->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('satuan');
        }
       
    }

}
?>