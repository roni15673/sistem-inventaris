<?php

class M_ukuran extends CI_model{

    public function getAllukuran(){
         return $this->db->get('ukuran')->result_array();
}

    public function tambah_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->insert('ukuran', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('ukuran', ['id' => $id]);
    }

    public function getAllukuranbyid($id){

       return $this->db->get_where('ukuran',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('ukuran', $data);
    }

    

}
