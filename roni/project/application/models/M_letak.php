<?php

class M_letak extends CI_model{

    public function getAllletak(){
         return $this->db->get('letak')->result_array();
}

    public function tambah_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->insert('letak', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('letak', ['id' => $id]);
    }

    public function getAllletakbyid($id){

       return $this->db->get_where('letak',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('letak', $data);
    }

    

}
