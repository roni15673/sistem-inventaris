<?php

class M_barang extends CI_model{

    public function getAllbarang(){
         return $this->db->get('barang')->result_array();
}

    public function tambah_data(){
        $data = [
            
            "nama" => $this->input->post('nama', true),
            "inventaris" => $this->input->post('inventaris', true),
            "letak" => $this->input->post('letak', true),
            "merek" => $this->input->post('merek', true),
            "asal" => $this->input->post('asal', true),
            "status" => $this->input->post('status', true),
            "keadaan" => $this->input->post('keadaan', true),
            "bahan" => $this->input->post('bahan', true),
            "satuan" => $this->input->post('satuan', true),
            "ukuran" => $this->input->post('ukuran', true),
            "tahun" => $this->input->post('tahun', true),
            "jumlah" => $this->input->post('jumlah', true),
            "harga" => $this->input->post('harga', true),
            "ket" => $this->input->post('ket', true)
        ];
        // print_r($data);exit;
        $this->db->insert('barang', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('barang', ['id' => $id]);
    }

    public function getAllbarangbyid($id){

       return $this->db->get_where('barang',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            
            "nama" => $this->input->post('nama', true),
            "inventaris" => $this->input->post('inventaris', true),
            "letak" => $this->input->post('letak', true),
            "merek" => $this->input->post('merek', true),
            "asal" => $this->input->post('asal', true),
            "status" => $this->input->post('status', true),
            "bahan" => $this->input->post('bahan', true),
            "satuan" => $this->input->post('satuan', true),
            "ukuran" => $this->input->post('ukuran', true),
            "tahun" => $this->input->post('tahun', true),
            "jumlah" => $this->input->post('jumlah', true),
            "harga" => $this->input->post('harga', true),
            "ket" => $this->input->post('ket', true)
        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('barang', $data);
    }

    

}
