<?php

class M_inventaris extends CI_model{

    public function getAllinventaris(){
         return $this->db->get('inventaris')->result_array();
}

    public function tambah_data(){
        $data = [
            "jenis" => $this->input->post('jenis', true)

        ];

        $this->db->insert('inventaris', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('inventaris', ['id' => $id]);
    }

    public function getAllinventarisbyid($id){

       return $this->db->get_where('inventaris',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "jenis" => $this->input->post('jenis', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('inventaris', $data);
    }

    

}
