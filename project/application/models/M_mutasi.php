<?php

class M_mutasi extends CI_model{

    public function getAllmutasi(){
         return $this->db->get('mutasi')->result_array();
}

    public function tambah_data(){
        $data = [

            "jenis_mutasi" => $this->input->post('jenis_mutasi', true),
            "inventaris" => $this->input->post('inventaris', true),
            "letak_lama" => $this->input->post('letak_lama', true),
            "letak_baru" => $this->input->post('letak_baru', true),
            "barang" => $this->input->post('barang', true), 
            "nama" => $this->input->post('nama', true),
            "tanggal" => $this->input->post('tanggal', true),
            "jumlah" => $this->input->post('jumlah', true),
            "ket" => $this->input->post('ket', true)

        ];

        $this->db->insert('mutasi', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('mutasi', ['id' => $id]);
    }

    public function getAllmutasibyid($id){

       return $this->db->get_where('mutasi',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [

            "jenis_mutasi" => $this->input->post('jenis_mutasi', true),
            "inventaris" => $this->input->post('inventaris', true),
            "letak_lama" => $this->input->post('letak_lama', true),
            "letak-baru" => $this->input->post('letak-baru', true),
            "barang" => $this->input->post('barang', true), 
            "nama" => $this->input->post('nama', true),
            "tanggal" => $this->input->post('tanggal', true),
            "jumlah" => $this->input->post('jumlah', true),
            "ket" => $this->input->post('ket', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('mutasi', $data);
    }

    

}
