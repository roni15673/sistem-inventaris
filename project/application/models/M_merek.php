<?php

class M_merek extends CI_model{

    public function getAllmerek(){
         return $this->db->get('merek')->result_array();
}

    public function tambah_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->insert('merek', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('merek', ['id' => $id]);
    }

    public function getAllmerekbyid($id){

       return $this->db->get_where('merek',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('merek', $data);
    }

    

}
