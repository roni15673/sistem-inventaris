<?php

class M_lokasi extends CI_model{

    public function getAlllokasi(){
         return $this->db->get('lokasi')->result_array();
}

    public function tambah_data(){
        $data = [
            "letak" => $this->input->post('letak', true),
            "barang" => $this->input->post('barang', true),
            "bangunan" => $this->input->post('bangunan', true),
            "kendaraan" => $this->input->post('kendaraan', true)

        ];

        $this->db->insert('lokasi', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('lokasi', ['id' => $id]);
    }

    public function getAlllokasibyid($id){

       return $this->db->get_where('lokasi',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
           "letak" => $this->input->post('letak', true),
            "barang" => $this->input->post('barang', true),
            "bangunan" => $this->input->post('bangunan', true),
            "kendaraan" => $this->input->post('kendaraan', true)


        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('lokasi', $data);
    }

    

}
