<?php

class M_status extends CI_model{

    public function getAllstatus(){
         return $this->db->get('status')->result_array();
}

    public function tambah_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->insert('status', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('status', ['id' => $id]);
    }

    public function getAllstatusbyid($id){

       return $this->db->get_where('status',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('status', $data);
    }

    

}
