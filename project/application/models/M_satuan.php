<?php

class M_satuan extends CI_model{

    public function getAllsatuan(){
         return $this->db->get('satuan')->result_array();
}

    public function tambah_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->insert('satuan', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('satuan', ['id' => $id]);
    }

    public function getAllsatuanbyid($id){

       return $this->db->get_where('satuan',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('satuan', $data);
    }

    

}
