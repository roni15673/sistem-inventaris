<?php

class M_user extends CI_model{

    public function getAlluser(){
         return $this->db->get('user')->result_array();
}

    public function tambah_data(){
        $data = [
            
            "nama" => $this->input->post('nama', true),
            "alamat" => $this->input->post('alamat', true),
            "agama" => $this->input->post('agama', true),
            "tempat_lahir" => $this->input->post('tempat_lahir', true),
            "tanggal_lahir" => $this->input->post('tanggal_lahir', true),
            "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
            "username" => $this->input->post('username', true),
            "password" => $this->input->post('password', true),
            "level" => $this->input->post('level', true)

        ];

        $this->db->insert('user', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('user', ['id' => $id]);
    }

    public function getAlluserbyid($id){

       return $this->db->get_where('user',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            
            "nama" => $this->input->post('nama', true),
            "alamat" => $this->input->post('alamat', true),
            "agama" => $this->input->post('agama', true),
            "tempat_lahir" => $this->input->post('tempat_lahir', true),
            "tanggal_lahir" => $this->input->post('tanggal_lahir', true),
            "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
            "username" => $this->input->post('username', true),
            "password" => $this->input->post('password', true)
        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('user', $data);
    }

    

}
