<?php

class M_bangunan extends CI_model{

    public function getAllbangunan(){
         return $this->db->get('bangunan')->result_array();
}

    public function tambah_data(){
        $data = [
            
            "nama" => $this->input->post('nama', true),
            "inventaris" => $this->input->post('inventaris', true),
            "letak" => $this->input->post('letak', true),
            "asal" => $this->input->post('asal', true),
            "status" => $this->input->post('status', true),
            "keadaan" => $this->input->post('keadaan', true),
            "tahun" => $this->input->post('tahun', true),
            "luas" => $this->input->post('luas', true),
            "harga" => $this->input->post('harga', true),
            "konstruksi" => $this->input->post('konstruksi', true)

           

        ];

        $this->db->insert('bangunan', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('bangunan', ['id' => $id]);
    }

    public function getAllbangunanbyid($id){

       return $this->db->get_where('bangunan',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [

             "nama" => $this->input->post('nama', true),
            "inventaris" => $this->input->post('inventaris', true),
            "letak" => $this->input->post('letak', true),
            "asal" => $this->input->post('asal', true),
            "status" => $this->input->post('status', true),
            "keadaan" => $this->input->post('keadaan', true),
            "tahun" => $this->input->post('tahun', true),
            "luas" => $this->input->post('luas', true),
            "harga" => $this->input->post('harga', true),
            "konstruksi" => $this->input->post('konstruksi', true)
        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('bangunan', $data);
    }

    

}
