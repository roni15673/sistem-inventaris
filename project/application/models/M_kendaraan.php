<?php

class M_kendaraan extends CI_model{

    public function getAllkendaraan(){
         return $this->db->get('kendaraan')->result_array();
}

    public function tambah_data(){
        $data = [
            "letak" => $this->input->post('letak', true),
            "inventaris" => $this->input->post('inventaris', true),
            "asal" => $this->input->post('asal', true),
            "status" => $this->input->post('status', true),
            "merek" => $this->input->post('merek', true), 
            "tahun" => $this->input->post('tahun', true),
            "harga" => $this->input->post('harga', true),
            "keadaan" => $this->input->post('keadaan', true),
            "no_rangka" => $this->input->post('no_rangka', true),
            "no_polisi" => $this->input->post('no_polisi', true),
            "no_bpkb" => $this->input->post('no_bpkb', true),
            "ket" => $this->input->post('ket', true)

        ];

        $this->db->insert('kendaraan', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('kendaraan', ['id' => $id]);
    }

    public function getAllkendaraanbyid($id){

       return $this->db->get_where('kendaraan',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
           "letak" => $this->input->post('letak', true),
            "inventaris" => $this->input->post('inventaris', true),
            "asal" => $this->input->post('asal', true),
            "status" => $this->input->post('status', true),
            "merek" => $this->input->post('merek', true), 
            "tahun" => $this->input->post('tahun', true),
            "harga" => $this->input->post('harga', true),
            "keadaan" => $this->input->post('keadaan', true),
            "no_rangka" => $this->input->post('no_rangka', true),
            "no_polisi" => $this->input->post('no_polisi', true),
            "no_bpkb" => $this->input->post('no_bpkb', true),
            "ket" => $this->input->post('ket', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('kendaraan', $data);
    }

    

}
