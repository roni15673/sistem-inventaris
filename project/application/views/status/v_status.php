   <style type="text/css">
  th{
    text-align: center;
  }
</style>
 <!-- page content -->
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

            <!--     <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Hello World <small>Graph title sub-title</small></h3>
                  </div>
                </div> -->
                <!-- flash data -->
                <?php if($this->session->flashdata('flash') ) :?>
    <div class="row ">
        <div class="col-md-6">
               <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               Data <strong>berhasil</strong> <?= $this->session->flashdata('flash'); ?>
              </div>
        </div>
      <?php endif;?>
      <!-- flash data -->

       <div class="title_right">
                <div class="col-md-3 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
                <div class="col-md-12 col-sm-9 col-xs-12">
                  <h4>Status</h4>
                    <div class="title_right">
                <div class="col-md-1 col-sm-5 col-xs-12  pull-right">
                  <?php
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/status/tambah" class="href btn btn-primary btn-sm"><i class="fa fa-plus-square"> Tambah Data</i></a>
         <?php } ?>
          
                    </span>
                  </div>
                </div>
              </div>
                
       <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
            $no =1;
            foreach($status as $u) : ?>
    <tr>
      <th scope="row"><?php print $no++ ?></th>
      <td><?php echo $u['nama'];?></td>
       <td><center>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/status/edit/<?= $u ['id'];?>" class="label label-success float-left"><i class="fa fa-edit"> Edit</i></a>
      <a href="<?php echo base_url();?>index.php/status/hapus/<?= $u ['id'];?>" class="label label-danger float-left"><i class="fa fa-trash"> Hapus</i></a>
     <?php } ?></center></td>
    </tr>
     <?php endforeach;?>
  </tbody>
  
</table>

                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <br>

          </div>
          </div>


                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

