

  <!-- hhhhhhhh -->
   <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-6">

            

              <div class="panel panel-default">
        <div class="panel-heading">Edit Data Lokasi</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
            <div class="card-body">
                <form action="" method="post">
                <div class="form-group">
                  <input type="hidden" name="id" value="<?php echo $lokasi['id'];?>">
                    <label for="letak">Letak<?php echo form_error('letak'); ?></label>
                    <select class="form-control" id="letak" name="letak">
                          <option disabled="" selected="">Pilih Letak</option>
                              <?php
                              foreach ($letak as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="barang">Barang<?php echo form_error('Barang'); ?></label>
                    <select class="form-control" id="barang" name="barang">
                          <option disabled="" selected="">Pilih barang</option>
                              <?php
                              foreach ($barang as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
               
                 <div class="form-group">
                    <label for="bangunan">Bangunan<?php echo form_error('Bangunan'); ?></label>
                    <select class="form-control" id="bangunan" name="bangunan">
                          <option disabled="" selected="">Pilih bangunan</option>
                              <?php
                              foreach ($bangunan as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['inventaris']; ?>"><?php echo $value['inventaris']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                  <div class="form-group">
                    <label for="kendaraan">Kendaraan<?php echo form_error('Kendaraan'); ?></label>
                    <select class="form-control" id="kendaraan" name="kendaraan">
                          <option disabled="" selected="">Pilih kendaraan</option>
                              <?php
                              foreach ($kendaraan as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['merek']; ?>"><?php echo $value['merek']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="row text-center">
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/lokasi" class="btn btn-primary float-right btn-sm"><i class="fa fa-arrow-circle-left  ">Back</i></a>
               </div>
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     