<style type="text/css">
  th{
    text-align: center;
  }
</style>
 <!-- page content -->
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

           <div class="flash-data" data-flashdata="<?php echo $this->session->flashdata('flash');?>"></div>
      <!-- flash data -->

       <div class="title_right">
                <div class="col-md-3 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
                <div class="col-md-12 col-sm-9 col-xs-12">
                  <h4><i class="fa fa-gavel"> Mutasi</i></h4>
                    <div class="title_right">
                <div class="col-md-1 col-sm-5 col-xs-12  pull-right">
                  <?php
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/mutasi/tambah" class="href btn btn-primary btn-sm" ><i class="fa fa-plus-square"> Tambah Data</i></a>
         <?php } ?>
          
                    </span>
                  </div>
                </div>
              </div>
                
       <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Jenis Mutasi</th>
      <th scope="col">Inventaris</th>
      <th scope="col">Letak Lama</th>
      <th scope="col">Letak Baru</th>
      <th scope="col">Barang</th>
      <th scope="col">Nama</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Jumlah</th>
      <th scope="col">keterangan</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
            $no =1;
            foreach($mutasi as $u) : ?>
    <tr>
      <th scope="row"><?php print $no++ ?></th>
      <td><?php echo $u['jenis_mutasi'];?></td>
      <td><?php echo $u['inventaris'];?></td>
      <td><?php echo $u['letak_lama'];?></td> 
      <td><?php echo $u['letak_baru'];?></td>
       <td><?php echo $u['barang'];?></td>
       <td><?php echo $u['nama'];?></td>
       <td><?php echo $u['tanggal'];?></td>
      <td><?php echo $u['jumlah'];?></td>
       <td><?php echo $u['ket'];?></td>
      
       <td><center>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/mutasi/edit/<?= $u ['id'];?>" class="label label-success float-left"><i class="fa fa-edit"> Edit</i></a>
      <a href="<?php echo base_url();?>index.php/mutasi/hapus/<?= $u ['id'];?>" class="label label-danger float-left"><i class="fa fa-trash"> Hapus</i></a>
     <?php } ?></center></td>
    </tr>
     <?php endforeach;?>
  </tbody>
  
</table>

                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <br>

          </div>
          </div>


                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

