

  <!-- hhhhhhhh -->
   <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-12">

            

              <div class="panel panel-default">
        <div class="panel-heading">Mutasi</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_mutasi">Jenis Mutasi</label>
                    <input type="text" class="form-control" id="jenis_mutasi" name="jenis_mutasi">
                     <small class="form-text text-danger"><?php echo form_error('Jenis Mutasi'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="inventaris">Inventaris<?php echo form_error('Inventaris'); ?></label>
                    <select class="form-control" id="inventaris" name="inventaris">
                          <option disabled="" selected="">Pilih inventaris</option>
                              <?php
                              foreach ($inventaris as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['jenis']; ?>"><?php echo $value['jenis']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
               
               <div class="form-group">
                    <label for="letak_lama">Letak Lama<?php echo form_error('Letak Lama'); ?></label>
                    <select class="form-control" id="letak_lama" name="letak_lama">
                          <option disabled="" selected="">Pilih letak lama</option>
                              <?php
                              foreach ($letak as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                  <div class="form-group">
                    <label for="letak_baru">Letak Baru<?php echo form_error('Letak Baru'); ?></label>
                    <select class="form-control" id="letak_baru" name="letak_baru">
                          <option disabled="" selected="">Pilih letak baru</option>
                              <?php
                              foreach ($letak as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                  </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="barang">Barang</label>
                    <input type="text" class="form-control" id="barang" name="barang">
                     <small class="form-text text-danger"><?php echo form_error('Barang'); ?></small>
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                     <small class="form-text text-danger"><?php echo form_error('Nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tanggal">Tanggal</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal">
                     <small class="form-text text-danger"><?php echo form_error('Tanggal'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="text" class="form-control" id="jumlah" name="jumlah">
                     <small class="form-text text-danger"><?php echo form_error('Jumlah'); ?></small>
                </div>
                   </div>
                 <div class="form-group">
                    <label for="ket">Keterangan</label>
                    <input type="text" class="form-control" id="ket" name="ket">
                     <small class="form-text text-danger"><?php echo form_error('Keterangan'); ?></small>
                </div>
                <div class="row text-center">
                    <div class="col">
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/mutasi" class="btn btn-primary float-right btn-sm"><i class="fa fa-arrow-circle-left  ">Back</i></a>
               </div>
               </div>
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     