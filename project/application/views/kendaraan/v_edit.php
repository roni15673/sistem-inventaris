

  <!-- hhhhhhhh -->
   <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-12">

            

              <div class="panel panel-default">
        <div class="panel-heading">Edit Data Kendaraan</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="col-md-6">
                    <input type="hidden" name="id" value="<?php echo $kendaraan['id']; ?>">
                <div class="form-group">
                    <label for="letak">Letak<?php echo form_error('Letak'); ?></label>
                    <select class="form-control" id="letak" name="letak">
                          <option disabled="" selected="">Pilih letak</option>
                              <?php
                              foreach ($letak as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
               <div class="form-group">
                    <label for="inventaris">Inventaris<?php echo form_error('Inventaris'); ?></label>
                    <select class="form-control" id="inventaris" name="inventaris">
                          <option disabled="" selected="">Pilih inventaris</option>
                              <?php
                              foreach ($inventaris as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['jenis']; ?>"><?php echo $value['jenis']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="asal">Asal</label>
                    <input type="text" class="form-control" id="asal" name="asal" value="<?php echo $kendaraan['asal']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('Asal'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="status">Status<?php echo form_error('Status'); ?></label>
                    <select class="form-control" id="status" name="status">
                          <option disabled="" selected="">Pilih status</option>
                              <?php
                              foreach ($status as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="merek">Merek</label>
                    <input type="text" class="form-control" id="merek" name="merek" value="<?php echo $kendaraan['merek']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('Merek'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" max="2019" min="1990" class="form-control" id="tahun" name="tahun" value="<?php echo $kendaraan['tahun']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('Tahun'); ?></small>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" value="<?php echo $kendaraan['harga']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('Harga'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="keadaan">Keadaan</label>
                    <input type="text" class="form-control" id="keadaan" name="keadaan" value="<?php echo $kendaraan['keadaan']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('Keadaan'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="no_rangka">No Rangka</label>
                    <input type="text" class="form-control" id="no_rangka" name="no_rangka" value="<?php echo $kendaraan['no_rangka']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('No Rangka'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="no_polisi">No Polisi</label>
                    <input type="text" class="form-control" id="no_polisi" name="no_polisi" value="<?php echo $kendaraan['no_polisi']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('No Polisi'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="no_bpkb">No BPKB</label>
                    <input type="text" class="form-control" id="no_bpkb" name="no_bpkb" value="<?php echo $kendaraan['no_bpkb']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('No BPKB'); ?></small>
                </div>
                <div class="form-group">
                    <label for="ket">Keterangan</label>
                    <input type="text" class="form-control" id="ket" name="ket" value="<?php echo $kendaraan['ket']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('Keterangan'); ?></small>
                </div>
                </div>
                <div class="row text-center">
                    <div class="col">
                 
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/kendaraan" class="btn btn-primary float-right btn-sm"><i class="fa fa-arrow-circle-left  ">Back</i></a>

                    </div>
                </div><!-- row_button -->
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     