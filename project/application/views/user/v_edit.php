
    <!-- edit -->

     <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-12">

            

              <div class="panel panel-default">
        <div class="panel-heading">Edit Data User</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
             <div class="card-body">
                  <form action="" method="post">
                    <div class="col-md-6">
                  <input type="hidden" name="id" value="<?php echo $user['id']; ?>">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $user['nama']; ?>">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $user['alamat']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('alamat'); ?></small>
                </div>
                <div class="form-group">
                    <label for="agama">Agama</label>
                    <input type="text" class="form-control" id="agama" name="agama" value="<?php echo $user['agama']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('agama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $user['tempat_lahir']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('tempat lahir'); ?></small>
                </div>
            </div><!-- form1 -->
            <div class="col-md-6">
                 <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="<?php echo $user['tanggal_lahir']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('tanggal lahir'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?php echo $user['jenis_kelamin']; ?>">
                      <option id="Laki-laki" value="Laki-laki">Laki-laki</option>
                      <option  id="jenis_kelamin" value="Perempuan">Perempuan</option>
                    </select>
                </div>
                 <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $user['username']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('username'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="<?php echo $user['password']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('password'); ?></small>
                </div>
            </div><!-- form2 -->
            <div class="row text-center">
                <div class="col">
                     <button type="submit" name="edit" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
             <a href="<?php echo site_url(); ?>/user" class="btn btn-primary float-right"><i class="fa fa-arrow-circle-left  ">Back</i></a>
         </div><!-- col_button -->
         </div><!-- row_button -->
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

