<style type="text/css">
  th{
    text-align: center;
  }
</style>
 <!-- page content -->
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

           <div class="flash-data" data-flashdata="<?php echo $this->session->flashdata('flash');?>"></div>
      <!-- flash data -->

       <div class="title_right">
                <div class="col-md-3 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
                <div class="col-md-12 col-sm-9 col-xs-12">
                  <h4><i class="fa fa-users"></i> Daftar User</h4>
                    <div class="title_right">
                <div class="col-md-1 col-sm-5 col-xs-12  pull-right">
                  <?php
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/user/tambah" class="href btn btn-primary btn-sm"><i class="fa fa-plus-square"> Tambah Data</i></a>
         <?php } ?>
          
                    </span>
                  </div>
                </div>
              </div>
                
       <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Alamat</th>
      <th scope="col">Agama</th>
      <th scope="col">Tempat Lahir</th>
      <th scope="col">Tanggal Lahir</th>
      <th scope="col">Jenis Kelamin</th>
      <th scope="col">Username</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
            $no =1;
            foreach($user as $u) : ?>
    <tr>
      <th scope="row"><?php print $no++ ?></th>
      <td><?php echo $u['nama'];?></td>
      <td><?php echo $u['alamat'];?></td>
      <td><?php echo $u['agama'];?></td>
      <td><?php echo $u['tempat_lahir'];?></td>
      <td><?php echo $u['tanggal_lahir'];?></td>
      <td><?php echo $u['jenis_kelamin'];?></td>
      <td><?php echo $u['username'];?></td>
       <td><center> <a href="<?php echo base_url();?>index.php/user/detail/<?= $u ['id'];?>" class="label label-primary float-left"><i class="fa fa-eye"> Detail</i></a>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/user/edit/<?= $u ['id'];?>" class="label label-success float-left"><i class="fa fa-edit"> Edit</i></a>
      <a href="<?php echo base_url();?>index.php/user/hapus/<?= $u ['id'];?>" class="label label-danger float-left"><i class="fa fa-trash"> Hapus</i></a>
     <?php } ?></center></td>
    </tr>
     <?php endforeach;?>
  </tbody>
  
</table>

                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <br>

          </div>
          </div>


                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

