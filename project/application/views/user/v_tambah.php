

  <!-- hhhhhhhh -->
   <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-12">

            

              <div class="panel panel-default">
        <div class="panel-heading">Tambah Data User</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
            <div class="card-body">
                <form action="" method="post">
                  <div class="col-md-6">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat">
                     <small class="form-text text-danger"><?php echo form_error('alamat'); ?></small>
                </div>
                <div class="form-group">
                    <label for="agama">Agama</label>
                    <select class="form-control" id="agama" name="agama">
                      <option disabled="" selected="">Pilih Agama</option>
                      <option id="agama" value="Islam">Islam</option>
                       <option id="agama" value="Kristen Protestan">Kristen Protestan</option>
                        <option id="agama" value="katolik">katolik</option>
                         <option id="agama" value="Hindu">Hindu</option>
                          <option id="agama" value="Buddha">Buddha</option>
                           <option id="agama" value="Kong Hu Cu">Kong Hu Cu</option>
                    </select>
                     <small class="form-text text-danger"><?php echo form_error('agama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="year" class="form-control" id="tempat_lahir" name="tempat_lahir">
                     <small class="form-text text-danger"><?php echo form_error('tempat lahir'); ?></small>
                </div>
              </div><!-- form1 -->
              <div class="col-md-6">
                 <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir">
                     <small class="form-text text-danger"><?php echo form_error('tanggal lahir'); ?></small>
                </div>
                 <div class="form-group">

                     <small class="form-text text-danger"><?php echo form_error('jenis kelamin'); ?></small>
                    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                      <option disabled="" selected="">Pilih Jenis Kelamin</option>
                       <option id="jenis_kelamin" value="Laki-laki">Laki-laki</option>
                        <option  id="jenis_kelamin" value="Perempuan">Perempuan</option>
                    </select>

                </div>
                 <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                     <small class="form-text text-danger"><?php echo form_error('username'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                     <small class="form-text text-danger"><?php echo form_error('password'); ?></small>
                </div>
              </div><!-- form2 -->
              <div class="row text-center">
                <div class="col">
                  <input type="hidden" name="level" value="user">
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/user" class="btn btn-primary float-right btn-sm"><i class="fa fa-arrow-circle-left  ">Back</i></a>
             </div><!-- col-button -->
             </div><!-- row_button -->
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     