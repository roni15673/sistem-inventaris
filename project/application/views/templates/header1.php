<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo base_url();?>asset/templates/production/images/logo.png" type="image/ico" />

    <title><?php echo $judul;?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>asset/templates/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>asset/templates/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>asset/templates/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url();?>asset/templates/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url();?>asset/templates/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url();?>asset/templates/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>asset/templates/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>asset/templates/build/css/custom.min.css" rel="stylesheet">

    <!-- easyui -->
   <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">
   <!--   <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/demo/demo.css">-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script> 
    <!-- end easyui -->

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-500px"></i> <span>Sistem Inventaris</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
            <?php if($this->session->userdata('permission') == true) { ?>
              <div class="profile_pic">
                <img src="<?php echo base_url();?>asset/templates/production/images/user.png" alt="..." class="img-circle profile_img"  data-toggle="modal" data-target="#profil">
              </div>
            <?php } ?>
             <?php if($this->session->userdata('permission') == false) { ?>
              <div class="profile_pic">
                <img src="<?php echo base_url();?>asset/templates/production/images/user.png" alt="..." class="img-circle profile_img"  data-toggle="modal" data-target="#profil2">
              </div>
            <?php } ?>
              <div class="profile_info">
                <span>Selamat Datang</span>
                <?php if($this->session->userdata('permission') == true) { ?>
                <h2  data-toggle="modal" data-target="#profil"><?php echo $this->session->userdata('nama'); ?></h2>
                <?php } ?>
                 <?php if($this->session->userdata('permission') == false) { ?>
                <h2  data-toggle="modal" data-target="#profil2">Silahkan login</h2>
                <?php } ?>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo site_url()?>/home"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li><a href="<?php echo site_url()?>/barang"><i class="fa fa-balance-scale"></i> Input Barang</a>
                  </li>
                  <li><a href="<?php echo site_url()?>/bangunan"><i class="fa fa-building"></i> Input Bangunan</a>
                  </li>
                  <li><a href="<?php echo site_url()?>/kendaraan"><i class="fa fa-truck"></i> Input Kendaraan</a>
                  </li>
                  <li><a href="<?php echo site_url()?>/Lokasi"><i class="fa fa-crosshairs"></i> Lokasi</a>
                  </li>
                  <li><a href="<?php echo site_url()?>/mutasi"><i class="fa fa-gavel"></i> Mutasi</a>
                  <li><a><i class="fa fa-table"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url();?>/user">Daftar User </a></li>
                      <li><a href="<?php echo site_url();?>/inventaris">Inventaris</a></li>
                      <li><a href="<?php echo site_url();?>/letak">Letak </a></li>
                      <li><a href="<?php echo site_url();?>/merek">Merek </a></li>
                      <li><a href="<?php echo site_url();?>/status">Status </a></li>
                      <li><a href="<?php echo site_url();?>/satuan">Satuan </a></li>
                      <li><a href="<?php echo site_url();?>/ukuran">ukuran </a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-bar-chart-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!-- 1 -->  <li><a>Barang<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                          <!-- 2 --><li><a>kendaraan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                    </ul>
                  </li>         
                   <li><a><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right" >Coming Soon</span></a></li>
                </ul>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <?php if($this->session->userdata('permission') == true ) { ?>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url();?>/login/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <?php } ?>
               <?php if($this->session->userdata('permission') == false ) { ?>
              <a data-toggle="tooltip" data-placement="top" title="Login" href="<?php echo site_url();?>/login">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <?php } ?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>asset/templates/production/images/user.png" alt=""><?php echo $this->session->userdata("nama"); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <?php if($this->session->userdata('permission') == true ) { ?>
                    <li><a href="javascript:;" data-toggle="modal" data-target="#profil"> Profile</a></li>
                  <?php } ?>
                   <?php if($this->session->userdata('permission') == false ) { ?>
                    <li><a href="javascript:;" data-toggle="modal" data-target="#profil2"> Profile</a></li>
                  <?php } ?>
                  <?php if($this->session->userdata('permission') == true ) { ?>
                    <li>
                      <a href="javascript:;"  data-toggle="modal" data-target="#settings">
                        <span>Settings</span>
                      </a>
                  <?php } ?>
                     <?php if($this->session->userdata('permission') == false ) { ?>
                    <li>
                      <a href="javascript:;"  data-toggle="modal" data-target="#profil2">
                        <span>Settings</span>
                      </a>
                  <?php } ?>
                    </li>
                    <li><a href="javascript:;"  data-toggle="modal" data-target="#settings">Help</a></li>
                    <?php if($this->session->userdata('permission') == true ) {?>
                    <li><a href="<?php echo site_url();?>/login/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    <?php } ?>
                     <?php if($this->session->userdata('permission') == false ) {?>
                    <li><a href="<?php echo site_url();?>/login"><i class="fa fa-sign-out pull-right"></i> Log in</a></li>
                    <?php } ?>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
      

     
    <!--   </div>
    </div -->>

  <!-- Modal_profil -->
<div class="modal fade" id="profil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Profil Saya</h4>
      </div>
      <div class="modal-body">
        <div class="list-group">
        <a href="#" class="list-group-item disabled">
        <h2> <?php echo $this->session->userdata("nama");?></h2>
        </a>
       Alamat <a href="#" class="list-group-item"><?php echo $this->session->userdata("alamat");?></a>
        Agama<a href="#" class="list-group-item"><?php echo $this->session->userdata("agama");?></a>
        Tempat Lahir<a href="#" class="list-group-item"><?php echo $this->session->userdata("tempat_lahir");?></a>
        Tanggal Lahir<a href="#" class="list-group-item"><?php echo $this->session->userdata("tanggal_lahir");?></a>
        Jens Kelamin<a href="#" class="list-group-item"><?php echo $this->session->userdata("jenis_kelamin");?></a>
         Username<a href="#" class="list-group-item"><?php echo $this->session->userdata("username");?></a>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal_setting -->
<div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Settings</h4>
      </div>
      <div class="modal-body">
       <span class="label label-success" >Coming Soon</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- belum login -->

<div class="modal fade" id="profil2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">!!!!!!!!!!!</h4>
      </div>
      <div class="modal-body">
       Silahkan login terlebih dahulu!!!!!!!!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <a href="<?php echo site_url()?>/login" class="btn btn-primary" >Login</a>
      </div>
    </div>
  </div>
</div>

