   <!-- footer content -->
        <footer>
          <div class="pull-right">
            &copy 2018 |<a href="#">Roni Angga</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>asset/templates/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>asset/templates/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>asset/templates/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>asset/templates/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>asset/templates/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>asset/templates/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>asset/templates/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>asset/templates/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>asset/templates/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>asset/templates/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>asset/templates/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>asset/templates/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>asset/templates/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>asset/templates/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>asset/templates/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>asset/templates/build/js/custom.min.js"></script>

    <!-- swet -->
    <script src="<?php echo base_url();?>/asset/sweet/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/asset/sweet/dist/sweetalert.css">
    <script src="<?php echo base_url();?>/asset/js/myscript.js" type="text/javascript"></script>
  




