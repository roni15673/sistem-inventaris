<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>asset/css/style.css">
    <!--  -->
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">

      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/easyui/themes/default/easyui.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/easyui/themes/icon.css">

      <!-- UIkit CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/css/uikit.min.css" />

     


    <title><?php echo $judul;?></title>
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">
  </head>
  <body>

  <!-- navbar -->
  <nav class="navbar navbar-expand-sm navbar-dark ">
  <a class="navbar-brand" href="<?php echo base_url();?>index.php/home"><img src="<?php echo base_url();?>asset/img/logo.png" alt="logo" width= 40px height=40px></td> <b>Sistem Inventaris</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<div class="container ">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <a class="nav-item nav-link" href="<?php echo base_url();?>index.php/home"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a>
      <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/barang"><i class="fa fa-balance-scale "></i>

Daftar Barang</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/user"><i class="fa fa-users"></i> Daftar User</a></li>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#"><i class="fa fa-ellipsis-v "></i></a>
      </li>
      
   
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-left"></i></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href=""><!-- <i class="fa fa-user"></i> -->Sistem Inventaris</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo site_url();?>/inventaris/"><!-- <i class="fa fa-wrench "></i> -->Inventaris</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/letak/"><!-- i class="fa fa-wrench "></i> -->Letak</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/merek/"><!-- <i class="fa fa-wrench "></i> -->Merek</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/status/"><!-- i class="fa fa-wrench "></i> -->Status</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/satuan"><!-- <i class="fa fa-wrench "></i> -->Satuan</a>
          <a class="dropdown-item" href="<?php echo site_url();?>/ukuran/"><!-- <i class="fa fa-wrench "></i> -->Ukuran</a>
        </div>
      </li>
          <?php 
        if($this->session->userdata('permission') ==false ) { ?>
          <div class="loginlogout">
        <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/login"><i class="fa fa-key"> Login</i></a></li> 
         <?php } ?>
          <div class="loginlogout">
         <?php 
        if($this->session->userdata('permission') ==true ) { ?>
        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('login/logout'); ?>"><i class="fa fa-undo"> Logout</i></a></li>
      </div>
        </div>
         <?php } ?>
        </ul>
    </div>
    </form>
  </div>
</nav>




 <script type="text/javascript" src="<?php echo base_url();?>asset/easyui/jquery.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>asset/easyui/jquery.easyui.min.js"></script>

