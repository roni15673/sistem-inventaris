<style type="text/css">
  th{
    text-align: center;
  }
</style>
 <!-- page content -->
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

           <div class="flash-data" data-flashdata="<?php echo $this->session->flashdata('flash');?>"></div>
      <!-- flash data -->

       <div class="title_right">
                <div class="col-md-3 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
                <div class="col-md-12 col-sm-9 col-xs-12">
                  <h4><i class="fa fa-building"></i> Daftar Bangunan</h4>
                    <div class="title_right">
                <div class="col-md-1 col-sm-5 col-xs-12  pull-right">
                  <?php
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/bangunan/tambah" class="href btn btn-primary btn-sm" ><i class="fa fa-plus-square"> Tambah Data</i></a>
         <?php } ?>
          
                    </span>
                  </div>
                </div>
              </div>
                
       <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Inventaris</th>
      <th scope="col">Letak</th>
      <th scope="col">Asal</th>
      <th scope="col">Status</th>
      <th scope="col">Keadaan</th>
      <th scope="col">tahun</th>
      <th scope="col">Luas</th>
      <th scope="col">Harga</th>
      <th scope="col">Konstruksi</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
            $no =1;
            foreach($bangunan as $u) : ?>
    <tr>
      <th scope="row"><?php print $no++ ?></th>
      <td><?php echo $u['nama'];?></td>
      <td><?php echo $u['inventaris'];?></td>
      <td><?php echo $u['letak'];?></td>
      <td><?php echo $u['asal'];?></td> 
      <td><?php echo $u['status'];?></td>
      <td><?php echo $u['keadaan'];?></td>
      <td><?php echo $u['tahun'];?></td>
      <td><?php echo $u['luas'];?></td>
      <td><?php echo $u['harga'];?></td>
      <td><?php echo $u['konstruksi'];?></td>
       <td><center><a href="<?php echo base_url();?>index.php/bangunan/detail/<?= $u ['id'];?>" class="label label-primary float-left"><i class="fa fa-eye"> Detail</i></a>
        <?php 
        if($this->session->userdata('permission') =='admin') { ?>
       <a href="<?php echo base_url();?>index.php/bangunan/edit/<?= $u ['id'];?>" class="label label-success float-left"><i class="fa fa-edit"> Edit</i></a>
      <a href="<?php echo base_url();?>index.php/bangunan/hapus/<?= $u ['id'];?>" class="label label-danger float-left"><i class="fa fa-trash"> Hapus</i></a>
     <?php } ?></center></td>
    </tr>
     <?php endforeach;?>
  </tbody>
  
</table>

                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <br>

          </div>
          </div>


                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

