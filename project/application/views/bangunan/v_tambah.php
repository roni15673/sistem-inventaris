

  <!-- hhhhhhhh -->
   <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-12">

            

              <div class="panel panel-default">
        <div class="panel-heading">Tambah Data Bangunan</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="col-md-6">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                     <small class="form-text text-danger"><?php echo form_error('Nama'); ?></small>
               </div>
               <div class="form-group">
                    <label for="inventaris">Inventaris<?php echo form_error('inventaris'); ?></label>
                    <select class="form-control" id="inventaris" name="inventaris">
                          <option disabled="" selected="">Pilih inventaris</option>
                              <?php
                              foreach ($inventaris as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['jenis']; ?>"><?php echo $value['jenis']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="letak">Letak<?php echo form_error('Letak'); ?></label>
                    <select class="form-control" id="letak" name="letak">
                          <option disabled="" selected="">Pilih letak</option>
                              <?php
                              foreach ($letak as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="asal">Asal</label>
                    <input type="text" class="form-control" id="asal" name="asal">
                     <small class="form-text text-danger"><?php echo form_error('Asal'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="status">Status<?php echo form_error('Status'); ?></label>
                    <select class="form-control" id="status" name="status">
                          <option disabled="" selected="">Pilih status</option>
                              <?php
                              foreach ($status as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
            </div><!-- form1 -->
            <div class="col-md-6">
                 <div class="form-group">
                    <label for="keadaan">Keadaan</label>
                    <input type="text" class="form-control" id="keadaan" name="keadaan">
                     <small class="form-text text-danger"><?php echo form_error('Keadaan'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" id="tahun" name="tahun" max="2019" min="1990">
                     <small class="form-text text-danger"><?php echo form_error('tahun'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="luas">Luas</label>
                    <input type="text" class="form-control" id="luas" name="luas">
                     <small class="form-text text-danger"><?php echo form_error('luas'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga">
                     <small class="form-text text-danger"><?php echo form_error('harga'); ?></small>
                </div>
                  <div class="form-group">
                    <label for="Konstruksi">konstruksi</label>
                    <input type="text" class="form-control" id="konstruksi" name="konstruksi">
                     <small class="form-text text-danger"><?php echo form_error('Konstruksi'); ?></small>
                </div>
                 </div><!-- form2 -->
                <div class="row text-center">
                    <div class="">
                     <button type="submit" name="tambah" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/bangunan" class="btn btn-primary float-right btn-sm"><i class="fa fa-arrow-circle-left  ">Back</i></a>
           </div><!-- col_button -->
           </div><!-- row_button -->
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     