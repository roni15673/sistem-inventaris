<style type="text/css">
  
</style>
 <!-- page content -->
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

            <h3>Laporan Barang</h3><br><br><br>

            <div class="row mt-5">
              <div class="col-sm-6">
                <form>
                  <div style="margin-bottom:20px">
                    <span class="btn btn-default"><input class="easyui-radiobutton" name="print" value=""> Excel                         <input class="easyui-radiobutton" name="print" value=""> Print PDF
                    </span>
                </div>
                   <div style="margin-bottom:20px">
                       <input class="easyui-radiobutton" name="periode" value=""> Bulanan
                  </div>
                  <div style="margin-bottom:20px">
                     <input class="easyui-radiobutton" name="periode" value=""> Harian
                   </div>

                </form>
              </div>
              <div class="col-sm-6">
                <button class="btn pull-right btn-primary">Proses</button>
                <button class="btn pull-right btn-success"><i class="fa fa-print"></i> Cetak</button>
    </form>
              </div>
            </div><br><br>
            <div class="row mt-3">
              <div class="col-sm-12">
                 <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Barang</th>
            <th scope="col">Gambar</th>
            <th scope="col">Inventaris</th>
            <th scope="col">Letak</th>
            <th scope="col">Merek</th>
            <th scope="col">Asal</th>
            <th scope="col">Status</th>
            <th scope="col">Bahan</th>
            <th scope="col">Ukuran</th>
            <th scope="col">Tahun</th>
            <th scope="col">Jumlah</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            <?php
                  $no =1;
                  foreach($barang as $u) : ?>
          <tr>
            <th scope="row"><?php print $no++ ?></th>
            <td><?php echo $u['nama'];?></td>
            <td><img src="<?php echo base_url();?>asset/upload/<?php echo $u['gambar'] ?>" alt="..." class="img-thumbnail" width= 100px></td>
            <td><?php echo $u['inventaris'];?></td>
            <td><?php echo $u['letak'];?></td>
            <td><?php echo $u['merek'];?></td>
            <td><?php echo $u['asal'];?></td>
            <td><?php echo $u['status'];?></td>
            <td><?php echo $u['bahan'];?></td>
            <td><?php echo $u['ukuran'];?></td>
            <td><?php echo $u['tahun'];?></td>
            <td><?php echo $u['jumlah'];?></td>
             <td><center> 
              <?php
              if($this->session->userdata('permission') =='admin') { ?>
             <a href="<?php echo base_url();?>index.php/barang/edit/<?= $u ['id'];?>" class="label label-success"><i class="fa fa-edit"> Edit</i></a>
            <a href="<?php echo base_url();?>index.php/barang/hapus/<?= $u ['id'];?>" class="label label-danger"><i class="fa fa-trash"> Hapus</i></a>
           <?php } ?></center></td>
          </tr>
           <?php endforeach;?>
        </tbody>
</table>
 </div>
  </div>   

</div>
<div class="clearfix"></div>
</div>
</div>
<br>
</div>
</div>


                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

     





  
