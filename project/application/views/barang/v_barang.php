<style type="text/css">
  th{
    text-align: center;
  }
</style>
 <!-- page content -->
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
                   <div class="flash-data" data-flashdata="<?php echo $this->session->flashdata('flash');?>"></div>
      <!-- flash data -->

       <div class="title_right">
                <div class="col-md-3 col-sm-5 col-xs-12 form-group pull-right top_search">
                                      <?php echo form_open('Barang/cari');?>
                  <div class="input-group">
                    <input type="text" class="form-control" name="cari" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                   <?php echo form_close();?>
                </div>
              </div>
                <div class="col-md-12 col-sm-9 col-xs-12">
                  <h4><i class="fa fa-balance-scale"></i> Daftar Barang</h4>
                    <div class="title_right">
                <div class="col-md-1 col-sm-5 col-xs-12 pull-right">
                  <?php
        if($this->session->userdata('permission') =='admin') { ?>
        <a href="<?php echo site_url();?>/barang/tambah" class="href btn btn-primary btn-sm "><i class="fa fa-plus-square"> Tambah Data
        </i></a>
         <a href="<?php echo site_url();?>/barang/tambah" class="href btn btn-success btn-sm"><i class="fa fa-print"> Print
        </i></a>
         <?php } ?>
          
                    </span>
                  </div>
                </div>
              </div>
                
        <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Barang</th>
            <th scope="col">Gambar</th>
            <th scope="col">Inventaris</th>
            <th scope="col">Letak</th>
            <th scope="col">Merek</th>
            <th scope="col">Asal</th>
            <th scope="col">Status</th>
            <th scope="col">Bahan</th>
            <th scope="col">Ukuran</th>
            <th scope="col">Tahun</th>
            <th scope="col">Jumlah</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            <?php
                  $no =1;
                  foreach($barang as $u) : ?>
          <tr>
            <th scope="row"><?php print $no++ ?></th>
            <td><?php echo $u['nama'];?></td>
            <td><img src="<?php echo base_url();?>asset/upload/<?php echo $u['gambar'] ?>" alt="..." class="img-thumbnail" width= 100px></td>
            <td><?php echo $u['inventaris'];?></td>
            <td><?php echo $u['letak'];?></td>
            <td><?php echo $u['merek'];?></td>
            <td><?php echo $u['asal'];?></td>
            <td><?php echo $u['status'];?></td>
            <td><?php echo $u['bahan'];?></td>
            <td><?php echo $u['ukuran'];?></td>
            <td><?php echo $u['tahun'];?></td>
            <td><?php echo $u['jumlah'];?></td>
             <td><center> <a href="<?php echo base_url();?>index.php/barang/detail/<?= $u ['id'];?>" class="label label-primary"><i class="fa fa-eye"> Detail</i></a>
              <?php
              if($this->session->userdata('permission') =='admin') { ?>
             <a href="<?php echo base_url();?>index.php/barang/edit/<?= $u ['id'];?>" class="label label-success"><i class="fa fa-edit"> Edit</i></a>
            <a href="<?php echo base_url();?>index.php/barang/hapus/<?= $u ['id'];?>" class="label label-danger"><i class="fa fa-trash"> Hapus</i></a>
           <?php } ?></center></td>
          </tr>
           <?php endforeach;?>

        </tbody>

</table>

                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <br>

          </div>
          </div>


                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

     