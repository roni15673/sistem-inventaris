    <!-- edit -->

     <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
        <div class="col-md-12">

            

              <div class="panel panel-default">
        <div class="panel-heading">Edit Data Barang</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
             <div class="card-body">
                  <form action="" method="post">
                    <div class="col-md-6">
                  <input type="hidden" name="id" value="<?php echo $barang['id']; ?>">
                <div class="form-group">
                    <label for="nama">Nama Barang</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $barang['nama']; ?>">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                    <label for="inventaris">Inventaris<?php echo form_error('inventaris'); ?></label>
                    <select class="form-control" id="inventaris" name="inventaris">
                              <?php foreach ($inventaris as $key => $value) : ?>
                                  <?php if($value == $barang['inventaris']) :?>
                            <option value="<?php echo $value['jenis']; ?>" selected><?php echo $value['jenis']; ?></option>
                             <?php else : ?>
                          <option value="<?php echo $value['jenis']; ?>"><?php echo $value['jenis']; ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="letak">Letak<?php echo form_error('letak'); ?></label>
                    <select class="form-control" id="letak" name="letak">
                              <?php foreach ($letak as $key => $value) : ?>
                                  <?php if($value == $barang['letak']) :?>
                            <option value="<?php echo $value['nama']; ?>" selected><?php echo $value['nama']; ?></option>
                             <?php else : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                    </select>
                 </div>
               <div class="form-group">
                    <label for="merek">Merek<?php echo form_error('merek'); ?></label>
                    <select class="form-control" id="merek" name="merek">
                              <?php foreach ($merek as $key => $value) : ?>
                                  <?php if($value == $barang['merek']) :?>
                            <option value="<?php echo $value['nama']; ?>" selected><?php echo $value['nama']; ?></option>
                             <?php else : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="asal">Asal</label>
                    <input type="text" class="form-control" id="asal" name="asal" value="<?php echo $barang['asal']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('asal'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="status">Status<?php echo form_error('status'); ?></label>
                    <select class="form-control" id="status" name="status">
                              <?php foreach ($status as $key => $value) : ?>
                                  <?php if($value == $barang['status']) :?>
                            <option value="<?php echo $value['nama']; ?>" selected><?php echo $value['nama']; ?></option>
                             <?php else : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                    </select>
                 </div>
               </div><!-- form1 -->
               <div class="col-md-6">
                 <div class="form-group">
                    <label for="bahan">Bahan</label>
                    <input type="text" class="form-control" id="bahan" name="bahan" value="<?php echo $barang['bahan']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('bahan'); ?></small>
                </div>
                <div class="form-group">
                    <label for="satuan">satuan<?php echo form_error('satuan'); ?></label>
                    <select class="form-control" id="satuan" name="satuan">
                              <?php foreach ($satuan as $key => $value) : ?>
                                  <?php if($value == $barang['satuan']) :?>
                            <option value="<?php echo $value['nama']; ?>" selected><?php echo $value['nama']; ?></option>
                             <?php else : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="ukuran">ukuran<?php echo form_error('ukuran'); ?></label>
                    <select class="form-control" id="ukuran" name="ukuran">
                              <?php foreach ($ukuran as $key => $value) : ?>
                                  <?php if($value == $barang['ukuran']) :?>
                            <option value="<?php echo $value['nama']; ?>" selected><?php echo $value['nama']; ?></option>
                             <?php else : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" min="2000" max="2019" class="form-control" id="tahun" name="tahun" value="<?php echo $barang['tahun']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('tahun'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" min="0" class="form-control" id="jumlah" name="jumlah" value="<?php echo $barang['jumlah']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('jumlah'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" value="<?php echo $barang['harga']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('harga'); ?></small>
                </div>
                 </div><!-- form2 -->
                 <div class="form-group">
                    <label for="ket">Keterangan</label>
                    <input type="text" class="form-control" id="ket" name="ket" value="<?php echo $barang['ket']; ?>">
                     <small class="form-text text-danger"><?php echo form_error('keterangan'); ?></small>
                </div>
                <div class="row mt-3 text-center">
                  <div class="col-md-12">
                     <button type="submit" name="edit" class="btn btn-primary ">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger ">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/barang" class="btn btn-primary float-right"><i class="fa fa-arrow-circle-left  ">Back</i></a>
             </div><!-- col buton -->
             </div><!-- button -->
            </div>
        </div>
  </div>
</div>

          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     