 <!-- page content -->
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

          <div class="row">
           <!--  <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph"> -->
                <div class="col-md-12 col-sm-9 col-xs-12">
                 <div class="row mt-3">
     

            

              <div class="panel panel-default">
        <div class="panel-heading">Tambah Data Barang</div>
        <div class="panel-body">
         <div class="card">
                 <div class="card-header">
                        
                </div>
                  <form action="" method="POST" enctype="multipart/form-data">
                       <div class="col-md-6">
                <div class="form-group">
                    <label for="nama">Nama Barang</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="gambar">Foto</label><br>
                    <input type="file" class="form-control-img" id="gambar" name="gambar">
                    <small class="form-text text-danger"><?php echo form_error('foto'); ?></small>
                </div>
                <div class="form-group">
                    <label for="inventaris">Inventaris<?php echo form_error('inventaris'); ?></label>
                    <select class="form-control" id="inventaris" name="inventaris">
                          <option disabled="" selected="">Pilih Inventaris</option>
                              <?php
                              foreach ($inventaris as $key => $value) : 
                              ?>
                          <option value="<?php echo $value['jenis']; ?>"><?php echo $value['jenis']; ?></option>
                              <?php endforeach; ?>
                    </select>
                 </div>
                  <div class="form-group">
                    <label for="letak">Letak<?php echo form_error('letak'); ?></label>
                    <select class="form-control" id="letak" name="letak">
                          <option disabled="" selected="">Pilih Letak</option>
                               <?php foreach ($letak as $key => $value ) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="merek">Merek<?php echo form_error('merek'); ?></label>
                    <select class="form-control" id="merek" name="merek">
                          <option disabled="" selected="">Pilih Merek</option>
                               <?php foreach ($merek as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="asal">Asal</label>
                    <input type="text" class="form-control" id="asal" name="asal">
                     <small class="form-text text-danger"><?php echo form_error('asal'); ?></small>
                </div>
                
                 <div class="form-group">
                    <label for="status">Status<?php echo form_error('status'); ?></label>
                    <select class="form-control" id="status" name="status">
                          <option disabled="" selected="">Pilih Status</option>
                               <?php foreach ($status as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                   <!-- form1 -->
              </div>
               <div class="col-md-6 ">
                 <div class="form-group">
                    <label for="bahan">Bahan</label>
                    <input type="text" class="form-control" id="bahan" name="bahan">
                     <small class="form-text text-danger"><?php echo form_error('bahan'); ?></small>
                </div>
                <div class="form-group">
                    <label for="satuan">Satuan<?php echo form_error('satuan'); ?></label>
                    <select class="form-control" id="satuan" name="satuan">
                          <option disabled="" selected="">Pilih Satuan</option>
                               <?php foreach ($satuan as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                <div class="form-group">
                    <label for="ukuran">Ukuran<?php echo form_error('ukuran'); ?></label>
                    <select class="form-control" id="ukuran" name="ukuran">
                          <option disabled="" selected="">Pilih Ukuran</option>
                               <?php foreach ($ukuran as $key => $value) : ?>
                          <option value="<?php echo $value['nama']; ?>"><?php echo $value['nama']; ?></option>
                                <?php endforeach; ?>
                    </select>
                 </div>
                 <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" id="tahun" name="tahun" min="2000" max="2019" >
                     <small class="form-text text-danger"><?php echo form_error('tahun'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" class="form-control" id="jumlah" name="jumlah" min="0" >
                     <small class="form-text text-danger"><?php echo form_error('jumlah'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" >
                     <small class="form-text text-danger"><?php echo form_error('harga'); ?></small>
                </div>
                 <div class="form-group">
                    <label for="ket">Keterangan</label>
                    <input type="textarea" class="form-control" id="ket" name="ket">
                     <small class="form-text text-danger"><?php echo form_error('keterangan'); ?></small>
                </div>
              </div><!-- form2 -->
              <div class="row text-center">
                <div class="col-md-12">
                     <button type="submit" name="tambah" class="btn btn-primary btn-sm">Simpan</button>
                      <button type="reset" name="hapus" class="btn btn-danger btn-sm">Hapus</button>
            </form>
               <a href="<?php echo site_url(); ?>/barang" class="btn btn-primary float-right btn-sm"><i class="fa fa-arrow-circle-left  ">Back</i></a>
             </div><!-- tutp button -->
            </div>
        </div><!-- col-md-6 -->
        </div>
  </div>
</div>
 
          
        </div>
    </div>

          </div>
          </div>


                <!-- End to do list -->
              </div>
                
                <!-- start of weather widget -->
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>

     