<!DOCTYPE html>
<html lang="en" id="home">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>My Portfolio</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="style.css">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
  <!-- jumbotron -->
  <div class="jumbotron text-center">
    <img src="<?php echo base_url(); ?>asset/img/logo.png" width="150px" height="150px" alt="ini gambar">
    <h1>Sistem Inventaris</h1>
  </div>

  <!-- akhir jumbotron -->

  <!-- about -->
  <section class="about" id="about">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="text-center">About</h2>
          <hr>
        </div>
      </div>


      <div class="row">
        <div class="col-sm-6">
          <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolores, sequi aliquam! Quos accusantium,
            adipisci cum aut consequuntur sint ut pariatur vitae iure aspernatur aliquid, aliquam, eligendi totam
            repellendus quae voluptatem!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda harum aut non quas expedita totam
            numquam. Quasi nisi commodi quia dolorem adipisci cum laudantium rerum nobis, veritatis provident!
            Voluptatem, voluptatibus.</p>
        </div>
        <div class="col-sm-6">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur voluptatem reiciendis consequatur ea
            aliquid assumenda maxime blanditiis animi sit ipsam quis officia commodi quas deserunt, excepturi incidunt
            veritatis velit dolorem.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem dolore doloremque, eligendi sed eaque eos
            non eius quod quibusdam excepturi explicabo commodi quia quasi sint vel, unde nihil ratione minima!</p>
        </div>
      </div>
    </div>
  </section>
  <!-- akhir about -->

  <!-- contact -->
  <section class="contact" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2>Contact</h2>
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <form action="">
            <div class="form-grup">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" placeholder="masukan nama">
            </div>
            <div class="form-grup">
              <label for="email">Email</label>
              <input type="email" class="form-control" placeholder="masukan email">
            </div>
            <div class="form-grup">
              <label for="telp">No Telepon</label>
              <input type="tel" class="form-control" id="telp" placeholder="masukan nomor telepon">
            </div><br>
            <label for="">Pilih Kategori</label>
            <select name="" id="" class="form-control">
              <option value="" readonly>-- Pilih Kategori --</option>
              <option value="">Web Desain</option>
              <option value="">web Development</option>
            </select><br>
            <label for="">Pesan</label>
            <div class="form-grup">
              <textarea name="" id="" cols="30" rows="10" placeholder="Masukan pesan"></textarea>
            </div><br>
            <button class="btn btn-primary">Kirim</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- akhir contact -->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-3.3.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>

  <script src="js/script.js"></script>
</body>

</html>