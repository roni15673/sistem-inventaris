<style type="text/css">
  .label{
    margin-left: 25px;
  }
</style>
  
 <div class="container">
        <div class="right_col" role="main" style="margin-top: -30px;">

   <!-- page content -->



            <div class="row mt-3">
                  <div class="col-md-6">

                        <div class="panel panel-default">
                            <div class="panel-heading">Inventaris</div>
                            <div class="panel-body">
                                <?php 
                              if($this->session->userdata('permission') =='admin') { ?>
                              <a href="" class="href btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_inventaris"><i class="fa fa-plus"></i></a>
                               <?php } ?>
                              <!-- conten  -->
                                <ul class="list-group">
                                  <?php foreach($inventaris as $u) : ?>
                                      <li class="list-group-item float-right"><?php echo $u['jenis'];?>
                                         <a href="<?php echo base_url();?>index.php/inventaris/hapus/<?= $u ['id'];?>" class="label label-danger" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>
                                      </li>
                                  <?php endforeach;?>
                                 </ul>
                            </div>
                        </div><!-- endpanel -->
                  </div><!-- col -->
                   <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Letak</div>
                            <div class="panel-body">
                              <!-- conten  -->
                                <?php 
                              if($this->session->userdata('permission') =='admin') { ?>
                              <a href="" class="href btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_letak"><i class="fa fa-plus"></i></a>
                               <?php } ?>
                              <ul class="list-group">
                                <?php foreach($letak as $u) : ?>
                                    <li class="list-group-item"><?php echo $u['nama'];?>
                                       <a href="<?php echo base_url();?>index.php/letak/hapus/<?= $u ['id'];?>" class="label label-danger float-right" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>
                                    </li>
                                <?php endforeach;?>
                                </ul>
                            </div>
                        </div><!-- endpanel -->
                  </div><!-- col -->
            </div><!-- row -->


                        <div class="row mt-3">
                  <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Merek</div>
                            <div class="panel-body">
                              <!-- conten  -->
                               <?php 
                              if($this->session->userdata('permission') =='admin') { ?>
                              <a href="" class="href btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_merek"><i class="fa fa-plus"></i></a>
                               <?php } ?>
                        <ul class="list-group">
                          <?php foreach($merek as $u) : ?>
                              <li class="list-group-item"><?php echo $u['nama'];?>
                                 <a href="<?php echo base_url();?>index.php/merek/hapus/<?= $u ['id'];?>" class="label label-danger float-right" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>
                              </li>
                          <?php endforeach;?>
                          </ul>
                            </div>
                        </div><!-- endpanel -->
                  </div><!-- col -->
                   <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Status</div>
                            <div class="panel-body">
                               <!-- conten  --> <?php 
                              if($this->session->userdata('permission') =='admin') { ?>
                              <a href="" class="href btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_status"><i class="fa fa-plus"></i></a>
                               <?php } ?>
                             <ul class="list-group">
                            <?php foreach($status as $u) : ?>
                                <li class="list-group-item"><?php echo $u['nama'];?>
                                   <a href="<?php echo base_url();?>index.php/status/hapus/<?= $u ['id'];?>" class="label label-danger float-right" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>

                                </li>
                            <?php endforeach;?>
                            </ul>
                            </div>
                        </div><!-- endpanel -->
                  </div><!-- col -->
            </div><!-- row -->

                        <div class="row mt-3">
                  <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Satuan</div>
                            <div class="panel-body">
                               <!-- conten  -->
                               <?php 
                              if($this->session->userdata('permission') =='admin') { ?>
                              <a href="" class="href btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_satuan"><i class="fa fa-plus"></i></a>
                               <?php } ?>
                              <ul class="list-group">
                        <?php foreach($satuan as $u) : ?>
                            <li class="list-group-item"><?php echo $u['nama'];?>
                               <a href="<?php echo base_url();?>index.php/satuan/hapus/<?= $u ['id'];?>" class="label label-danger float-right" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                            </div>
                        </div><!-- endpanel -->
                  </div><!-- col -->
                   <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">ukuran</div>
                            <div class="panel-body">
                               <!-- conten  -->
                                <?php 
                              if($this->session->userdata('permission') =='admin') { ?>
                              <a href="" class="href btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_ukuran"><i class="fa fa-plus"></i></a>
                               <?php } ?>
                             <ul class="list-group">
                              <?php foreach($ukuran as $u) : ?>
                                  <li class="list-group-item"><?php echo $u['nama'];?>
                                     <a href="<?php echo base_url();?>index.php/ukuran/hapus/<?= $u ['id'];?>" class="label label-danger float-right" onclick="return confirm('apakah anda akan menghapus?');">Hapus</a>
                                  </li>
                              <?php endforeach;?>
                              </ul>
                            </div>
                        </div><!-- endpanel -->
                  </div><!-- col -->
            </div><!-- row -->

         </div>
</div>
    

    



<!-- #inventaris -->
<!-- tambah -->
<div class="modal fade" id="tambah_inventaris" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Inventaris</h4>
      </div>
      <div class="modal-body">
         <form action="<?php echo site_url()?>/inventaris/tambah" method="post">
                <div class="form-group">
                    <label for="jenis">Jenis</label>
                    <input type="text" class="form-control" id="jenis" name="jenis">
                    <small class="form-text text-danger"><?php echo form_error('Jenis'); ?></small>
                </div>       
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- # -->
<!-- #letak -->
<!-- tambah -->
<div class="modal fade" id="tambah_letak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Letak</h4>
      </div>
      <div class="modal-body">
         <form action="<?php echo site_url()?>/letak/tambah" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>       
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- # -->

<!-- merek -->
<div class="modal fade" id="tambah_merek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Merek</h4>
      </div>
      <div class="modal-body">
         <form action="<?php echo site_url()?>/merek/tambah" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>       
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- # -->

<!-- status-->
<div class="modal fade" id="tambah_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Status</h4>
      </div>
      <div class="modal-body">
         <form action="<?php echo site_url()?>/status/tambah" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>       
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- # -->
<!-- ukuran-->
<div class="modal fade" id="tambah_ukuran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ukuran</h4>
      </div>
      <div class="modal-body">
         <form action="<?php echo site_url()?>/ukuran/tambah" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>       
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- # -->
<!-- satuan-->
<div class="modal fade" id="tambah_satuan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Satuan</h4>
      </div>
      <div class="modal-body">
         <form action="<?php echo site_url()?>/satuan/tambah" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                    <small class="form-text text-danger"><?php echo form_error('nama'); ?></small>
                </div>       
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- # -->