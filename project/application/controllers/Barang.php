<?php

class Barang extends CI_Controller{

    public function __construct(){
        parent:: __construct();
        
        $this->load->model('m_barang');
        $this->load->model('m_inventaris');
        $this->load->model('m_letak');
        $this->load->model('m_merek');
        $this->load->model('m_status');
        $this->load->model('m_satuan');
        $this->load->model('m_ukuran');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Daftar Barang';
        $data['barang'] = $this->m_barang->getAllbarang();
        $this->load->view('templates/header1', $data);
        $this->load->view('barang/v_barang');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        $data['judul'] = 'Tambah data';
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['letak'] = $this->m_letak->getAllletak();
        $data['merek'] = $this->m_merek->getAllmerek();
        $data['status'] = $this->m_status->getAllstatus();
        $data['satuan'] = $this->m_satuan->getAllsatuan();
        $data['ukuran'] = $this->m_ukuran->getAllukuran();
       
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('merek', 'Merek', 'required');
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('bahan', 'Bahan', 'required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        $this->form_validation->set_rules('ukuran', 'Ukuran', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required');

        if($this->form_validation->run() == FALSE){
            $this->load->view('templates/header1', $data);
            $this->load->view('barang/v_tambah', $data);
            $this->load->view('templates/footer');
        }else{

                $config['upload_path']          = './asset/upload/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1024;

                $this->load->library('upload');
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('foto'))
                {
                        echo $this->upload->display_errors();
                }
                else
                {
                        $upload_data = $this->upload->data();
                        $gambar = $upload_data['file_name'];
                }


                    $data = [
                        "nama" => $this->input->post('nama', true),
                        "gambar" => $gambar, 
                        "inventaris" => $this->input->post('inventaris', true),
                        "letak" => $this->input->post('letak', true),
                        "merek" => $this->input->post('merek', true),
                        "asal" => $this->input->post('asal', true),
                        "status" => $this->input->post('status', true),
                        "bahan" => $this->input->post('bahan', true),
                        "satuan" => $this->input->post('satuan', true),
                        "ukuran" => $this->input->post('ukuran', true),
                        "tahun" => $this->input->post('tahun', true),
                        "jumlah" => $this->input->post('jumlah', true),
                        "harga" => $this->input->post('harga', true),
                        "ket" => $this->input->post('ket', true)
                    ];

                    $this->m_barang->tambah_data($data);
                    $this->session->set_flashdata('flash','Ditambahkan');
                    redirect('barang');
                //}
        }
    }
    public function hapus($id){

        $this->m_barang->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('barang');

    }

    public function detail($id){

        $data['judul'] = 'Detail';
        $data['barang'] = $this->m_barang->getAllbarangbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('barang/detail');
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['barang'] = $this->m_barang->getAllbarangbyid($id);
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['letak'] = $this->m_letak->getAllletak();
        $data['merek'] = $this->m_merek->getAllmerek();
        $data['status'] = $this->m_status->getAllstatus();
        $data['satuan'] = $this->m_satuan->getAllsatuan();
        $data['ukuran'] = $this->m_ukuran->getAllukuran();

       $this->form_validation->set_rules('nama', 'Nama', 'required');
        // $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('merek', 'Merek', 'required');
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('bahan', 'Bahan', 'required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        $this->form_validation->set_rules('ukuran', 'Ukuran', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('barang/v_edit', $data);
            $this->load->view('templates/footer');
        } else {

             $config['upload_path']          = './asset/upload/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1024;

                $this->load->library('upload');
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('foto'))
                {
                        echo $this->upload->display_errors();
                }
                else
                {
                        $upload_data = $this->upload->data();
                        $gambar = $upload_data['file_name'];
                }
                
            $this->m_barang->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('barang');
        }
       
    }
    function cari(){

        $data['judul'] = 'Daftar Barang';
        $data['cari']=$this->input->post('cari');

        $data['barang']=$this->m_barang->cari($data['cari']);
        $this->load->view('templates/header1', $data);
        $this->load->view('barang/v_barang');
        $this->load->view('templates/footer');

    }

    public function report(){

        $data['judul'] = 'Laporan Barang';
        $data['barang'] = $this->m_barang->getAllbarang();

        $this->load->view('templates/header1', $data);
        $this->load->view('barang/report', $data);
        $this->load->view('templates/footer');
    }

}
?>