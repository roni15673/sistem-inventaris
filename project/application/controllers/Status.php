<?php

class Status extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_status');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Status';
        $data['status'] = $this->m_status->getAllstatus();
        $this->load->view('templates/header1', $data);
        $this->load->view('status/v_status');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('status/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_status->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('status');
        }
       
    }

    public function hapus($id){

        $this->m_status->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('status');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['status'] = $this->m_status->getAllstatusbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('status/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['status'] = $this->m_status->getAllstatusbyid($id);

       $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('status/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_status->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('status');
        }
       
    }

}
?>