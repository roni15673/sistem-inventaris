<?php

class User extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_user');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Daftar user';
        $data['user'] = $this->m_user->getAlluser();
        $this->load->view('templates/header1', $data);
        $this->load->view('user/v_user');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('agama', 'Agama', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat_lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal_lahir', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis_kelamin', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('user/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_user->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('user');
        }
       
    }

    public function hapus($id){

        $this->m_user->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('user');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['user'] = $this->m_user->getAlluserbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('user/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['user'] = $this->m_user->getAlluserbyid($id);

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('agama', 'Agama', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat_lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal_lahir', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis_kelamin', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('user/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_user->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('user');
        }
       
    }

}
?>