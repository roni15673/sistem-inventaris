<?php

class Letak extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_letak');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Letak';
        $data['letak'] = $this->m_letak->getAllletak();
        $this->load->view('templates/header1', $data);
        $this->load->view('letak/v_letak');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('letak/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_letak->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('letak');
        }
       
    }

    public function hapus($id){

        $this->m_letak->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('letak');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['letak'] = $this->m_letak->getAllletakbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('letak/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['letak'] = $this->m_letak->getAllletakbyid($id);

       $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('letak/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_letak->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('letak');
        }
       
    }

}
?>