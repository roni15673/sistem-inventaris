<?php 
 
class Login extends CI_Controller{
 
   public function __construct(){
        parent::__construct();      
        $this->load->model('m_login');
        $this->load->model('m_user');
 
    }
 
   public function index(){

        $data['judul'] = 'Login | Sistem Invemtaris';
        // $this->load->view('templates/header', $data);
        $this->load->view('login/v_login', $data);
         // $this->load->view('templates/footer');
    }
 
    public function aksi_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => $password
            );
        $cek = $this->m_login->cek_login("user",$where)->num_rows();
        
        if($cek > 0){
            $data = $this->m_login->cek_login("user",$where)->result_array();
            $data_session = array(
                
                'username' => $username,
                'permission' => $data[0]['level'],
                'nama' => $data[0]['nama'],
                'alamat' => $data[0]['alamat'],
                'agama' => $data[0]['agama'],
                'tempat_lahir' => $data[0]['tempat_lahir'],
                'tanggal_lahir' => $data[0]['tanggal_lahir'],
                'jenis_kelamin' => $data[0]['jenis_kelamin'],
                'username' => $data[0]['username'],
                'status' => "login"
                );
 
            $this->session->set_userdata($data_session);
                 redirect(site_url('home'));
 
        }else{
             $this->session->set_flashdata('flash','username atau password salah');
             redirect('login');
            
        }
    }
 
    public function logout(){
        session_destroy();
        redirect(site_url('login'));
    }
}









