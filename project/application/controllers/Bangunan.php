<?php

class Bangunan extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_bangunan');
        $this->load->model('m_inventaris');
        $this->load->model('m_letak');
        $this->load->model('m_status');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Bangunan';
        $data['bangunan'] = $this->m_bangunan->getAllbangunan();
        $this->load->view('templates/header1', $data);
        $this->load->view('bangunan/v_bangunan');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['letak'] = $this->m_letak->getAllletak();
        $data['status'] = $this->m_status->getAllstatus();
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('keadaan', 'Keadaan', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('luas', 'Luas', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('konstruksi', 'Konstruksi', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('bangunan/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_bangunan->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('bangunan');
        }
       
    }

    public function hapus($id){

        $this->m_bangunan->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('bangunan');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['bangunan'] = $this->m_bangunan->getAllbangunanbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('bangunan/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['bangunan'] = $this->m_bangunan->getAllbangunanbyid($id);
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['letak'] = $this->m_letak->getAllletak();
        $data['status'] = $this->m_status->getAllstatus();

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('keadaan', 'Keadaan', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('luas', 'Luas', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('konstruksi', 'Konstruksi', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('bangunan/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_bangunan->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('bangunan');
        }
       
    }

}
?>