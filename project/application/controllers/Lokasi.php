<?php

class Lokasi extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_lokasi');
        $this->load->model('m_letak');
        $this->load->model('m_barang');
        $this->load->model('m_bangunan');
        $this->load->model('m_kendaraan');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Lokasi';
        $data['lokasi'] = $this->m_lokasi->getAlllokasi();
        $this->load->view('templates/header1', $data);
        $this->load->view('lokasi/v_lokasi');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
        $data['letak'] = $this->m_letak->getAllletak();
        $data['barang'] = $this->m_barang->getAllbarang();
        $data['bangunan'] = $this->m_bangunan->getAllbangunan();
        $data['kendaraan'] = $this->m_kendaraan->getAllkendaraan();
    
    
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('barang', 'Barang', 'required');        
        $this->form_validation->set_rules('bangunan', 'Bangunan', 'required');
        $this->form_validation->set_rules('kendaraan', 'Kendaraan', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('lokasi/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_lokasi->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('lokasi');
        }
       
    }

    public function hapus($id){

        $this->m_lokasi->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('lokasi');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['lokasi'] = $this->m_lokasi->getAlllokasibyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('lokasi/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['lokasi'] = $this->m_lokasi->getAlllokasibyid($id);
        $data['letak'] = $this->m_letak->getAllletak();
        $data['barang'] = $this->m_barang->getAllbarang();
        $data['bangunan'] = $this->m_bangunan->getAllbangunan();
        $data['kendaraan'] = $this->m_kendaraan->getAllkendaraan();

         $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('barang', 'Barang', 'required');        
        $this->form_validation->set_rules('bangunan', 'Bangunan', 'required');
        $this->form_validation->set_rules('kendaraan', 'Kendaraan', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('lokasi/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_lokasi->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('lokasi');
        }
       
    }

}
?>