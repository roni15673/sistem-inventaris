<?php

class Merek extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_merek');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Merek';
        $data['merek'] = $this->m_merek->getAllmerek();
        $this->load->view('templates/header1', $data);
        $this->load->view('merek/v_merek');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('merek/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_merek->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('merek');
        }
       
    }

    public function hapus($id){

        $this->m_merek->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('merek');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['merek'] = $this->m_merek->getAllmerekbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('merek/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['merek'] = $this->m_merek->getAllmerekbyid($id);

       $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('merek/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_merek->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('merek');
        }
       
    }

}
?>