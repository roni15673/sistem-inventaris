<?php

class Home extends CI_Controller{

    public function __construct(){
        parent::__construct();
         $this->load->model('m_login');
    }


    public function index($nama = '')
    {
            $data['judul'] = 'Home';
            $this->load->view('templates/header1', $data);
            $this->load->view('home/v_home');
            $this->load->view('templates/footer');
    }


}

?>