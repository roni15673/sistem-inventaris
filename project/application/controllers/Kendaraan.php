<?php

class Kendaraan extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_kendaraan');
        $this->load->model('m_letak');
        $this->load->model('m_inventaris');
        $this->load->model('m_status');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Kendaraan';
        $data['kendaraan'] = $this->m_kendaraan->getAllkendaraan();
        $this->load->view('templates/header1', $data);
        $this->load->view('kendaraan/v_kendaraan');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
        $data['letak'] = $this->m_letak->getAllletak();
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['status'] = $this->m_status->getAllstatus();
    
        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');        
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('merek', 'Merek', 'required'); 
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('keadaan', 'Keadaan', 'required');
        $this->form_validation->set_rules('no_rangka', 'No_rangka', 'required');
        $this->form_validation->set_rules('no_polisi', 'No_polisi', 'required');   
        $this->form_validation->set_rules('no_bpkb', 'No_bpkb', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required'); 
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('kendaraan/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_kendaraan->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('kendaraan');
        }
       
    }

    public function hapus($id){

        $this->m_kendaraan->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('kendaraan');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['kendaraan'] = $this->m_kendaraan->getAllkendaraanbyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('kendaraan/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['kendaraan'] = $this->m_kendaraan->getAllkendaraanbyid($id);
        $data['letak'] = $this->m_letak->getAllletak();
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['status'] = $this->m_status->getAllstatus();

        $this->form_validation->set_rules('letak', 'Letak', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');        
        $this->form_validation->set_rules('asal', 'Asal', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('merek', 'Merek', 'required'); 
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('keadaan', 'Keadaan', 'required');
        $this->form_validation->set_rules('no_rangka', 'No_rangka', 'required');
        $this->form_validation->set_rules('no_polisi', 'No_polisi', 'required');   
        $this->form_validation->set_rules('no_bpkb', 'No_bpkb', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required'); 
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('kendaraan/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_kendaraan->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('kendaraan');
        }
       
    }

}
?>