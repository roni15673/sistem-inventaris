<?php

class Sistem extends CI_Controller{

    public function __construct(){
        parent::__construct();
        
    }


    public function index($nama = '')
    {

        $data['judul'] = 'Sistem Inventaris';
        $data['nama'] = $nama;
         $this->load->view('templates/header', $data);
         $this->load->view('sistem/v_sistem',$data);
         $this->load->view('templates/footer');
    }


}

?>