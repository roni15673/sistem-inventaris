<?php

class Mutasi extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_mutasi');
        $this->load->model('m_inventaris');
        $this->load->model('m_barang');
        $this->load->model('m_letak');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Mutasi';
        $data['mutasi'] = $this->m_mutasi->getAllmutasi();
        $this->load->view('templates/header1', $data);
        $this->load->view('mutasi/v_mutasi');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['barang'] = $this->m_barang->getAllbarang();
        $data['letak'] = $this->m_letak->getAllletak();
    
        $this->form_validation->set_rules('jenis_mutasi', 'Jenis_mutasi', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak_lama', 'Letak_lama', 'required');
        $this->form_validation->set_rules('letak_baru', 'Letak_baru', 'required');
        $this->form_validation->set_rules('barang', 'Barang', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('mutasi/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_mutasi->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('mutasi');
        }
       
    }

    public function hapus($id){

        $this->m_mutasi->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('mutasi');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['mutasi'] = $this->m_mutasi->getAllmutasibyid($id);
        $this->load->view('templates/header1', $data);
        $this->load->view('mutasi/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['mutasi'] = $this->m_mutasi->getAllmutasibyid($id);
        $data['inventaris'] = $this->m_inventaris->getAllinventaris();
        $data['barang'] = $this->m_barang->getAllbarang();
        $data['letak'] = $this->m_letak->getAllletak();

        
        $this->form_validation->set_rules('jenis_mutasi', 'Jenis_mutasi', 'required');
        $this->form_validation->set_rules('inventaris', 'Inventaris', 'required');
        $this->form_validation->set_rules('letak_lama', 'Letak_lama', 'required');
        $this->form_validation->set_rules('letak_baru', 'Letak_baru', 'required');
        $this->form_validation->set_rules('barang', 'Barang', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('ket', 'Ket', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header1', $data);
            $this->load->view('mutasi/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_mutasi->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('mutasi');
        }
       
    }

}
?>