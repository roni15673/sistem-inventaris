-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2019 at 04:30 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `bangunan`
--

CREATE TABLE `bangunan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `inventaris` varchar(255) NOT NULL,
  `letak` varchar(255) NOT NULL,
  `asal` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `keadaan` varchar(2555) NOT NULL,
  `tahun` int(11) NOT NULL,
  `luas` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `konstruksi` varchar(2555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bangunan`
--

INSERT INTO `bangunan` (`id`, `nama`, `inventaris`, `letak`, `asal`, `status`, `keadaan`, `tahun`, `luas`, `harga`, `konstruksi`) VALUES
(2, 'ruang1', 'alat1', 'Luar', 'aaa', 'Rusak', 'z', 2003, 'zz', 75000, 'aaa'),
(3, 'ruang2', 'wc', 'z', 'aaa', 'z', 'z', 2222, 'zz', 1111, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `inventaris` varchar(255) NOT NULL,
  `letak` varchar(255) NOT NULL,
  `merek` varchar(255) NOT NULL,
  `asal` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `bahan` varchar(255) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` varchar(11) NOT NULL,
  `ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama`, `gambar`, `inventaris`, `letak`, `merek`, `asal`, `status`, `bahan`, `satuan`, `ukuran`, `tahun`, `jumlah`, `harga`, `ket`) VALUES
(25, 'meja besi', 'year_of_the_tiger-wallpaper-1366x768.jpg', 'Perabotan', 'Luar', 'Samsung', 'Jepara', 'Baru', 'Kayu Jati', 'Pcs', 'Standard', 2007, 5, '1000.000', 'awet'),
(26, 'Lemari', 'year_of_the_tiger-wallpaper-1366x7682.jpg', 'Perabotan', 'Luar', 'Samsung', 'aaa', 'Baru', 'aaa', 'Pcs', 'Kecil', 2002, 2, '1111', 'jangan di pindah'),
(29, 'Kipas Angin', 'ubuntu_bone-wallpaper-1366x768.jpg', 'Perabotan', 'Luar', 'Samsung', 'aaa', 'Baru', 'aaa', 'Pcs', 'Kecil', 2000, 1, '1111', 'aaaaa'),
(30, 'meja', 'year_of_the_tiger-wallpaper-1366x7683.jpg', 'Perabotan', 'Luar', 'Samsung', 'aaa', 'Baru', 'aaa', 'Pcs', 'Kecil', 2002, 2, '1111', 'aaaaa'),
(32, 'kursi', 'download.jpg', 'Perabotan', 'Lantai 2', 'Mabloo', 'Riau', 'Baru', 'Kayu Jati', 'Pcs', 'Standard', 2018, 3, '75000', 'Kursi nyaman');

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id` int(11) NOT NULL,
  `jenis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id`, `jenis`) VALUES
(3, 'Perabotan'),
(8, 'aa'),
(10, 'alat ');

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL,
  `letak` varchar(255) NOT NULL,
  `inventaris` varchar(255) NOT NULL,
  `asal` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `merek` varchar(2555) NOT NULL,
  `tahun` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `keadaan` varchar(255) NOT NULL,
  `no_rangka` varchar(255) NOT NULL,
  `no_polisi` varchar(255) NOT NULL,
  `no_bpkb` varchar(255) NOT NULL,
  `ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `letak`, `inventaris`, `asal`, `status`, `merek`, `tahun`, `harga`, `keadaan`, `no_rangka`, `no_polisi`, `no_bpkb`, `ket`) VALUES
(1, 'aaab', 'aa', 'aa', 'aa', 'aaa', 11, 111, 'aaa', '121efsd465643334', 'R 4560 ZL', 'fdfss5fs', 'ffffff1'),
(2, 'z', 'z', 'Riau', 'z', 'mmm', 0, 0, 'mm', '77989', '7979yy9', '7979', '7ihhkh');

-- --------------------------------------------------------

--
-- Table structure for table `letak`
--

CREATE TABLE `letak` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `letak`
--

INSERT INTO `letak` (`id`, `nama`) VALUES
(5, 'Luar'),
(11, 'kursi'),
(12, 'kursi');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `letak` varchar(255) NOT NULL,
  `barang` varchar(255) NOT NULL,
  `bangunan` varchar(255) NOT NULL,
  `kendaraan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `letak`, `barang`, `bangunan`, `kendaraan`) VALUES
(1, 'Luar', 'Lemari', 'zz', 'mmm'),
(2, 'Luar', 'meja besi', 'zz', 'aaa'),
(5, 'Luar', 'meja besi', 'wc', 'mmm');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nrp` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `jurusan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nama`, `nrp`, `email`, `jurusan`) VALUES
(1, 'Roni', 15673, 'roniangga@gmail.com', 'RPL'),
(2, 'Bintang subekti', 15674, 'bintangsubekti@gmail.com', 'Pemasaran'),
(6, 'Bagus', 10, 'roniangga04@gmail.com', 'TKJ'),
(10, 'asa', 9, 'ronyangga33@gmail.com', ''),
(13, 'budi', 9, 'asas@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `merek`
--

CREATE TABLE `merek` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merek`
--

INSERT INTO `merek` (`id`, `nama`) VALUES
(3, 'Samsung'),
(4, 'Lenovo'),
(5, 'LG'),
(6, 'Mabloo'),
(7, 'Maspion');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi`
--

CREATE TABLE `mutasi` (
  `id` int(11) NOT NULL,
  `jenis_mutasi` varchar(255) NOT NULL,
  `inventaris` varchar(255) NOT NULL,
  `letak_lama` varchar(255) NOT NULL,
  `letak_baru` varchar(255) NOT NULL,
  `barang` varchar(2555) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi`
--

INSERT INTO `mutasi` (`id`, `jenis_mutasi`, `inventaris`, `letak_lama`, `letak_baru`, `barang`, `nama`, `tanggal`, `jumlah`, `ket`) VALUES
(2, 'mutasi', 'alat1', 'Lantai 3', 'Luar', 'a', 'meja', '2019-01-16', 3, 'Kursi nyaman');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama`) VALUES
(3, 'Pcs'),
(4, 'Ikat'),
(5, 'Lusin'),
(6, 'Kodi');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `nama`) VALUES
(1, 'Baru'),
(2, 'Lama'),
(3, 'Rusak');

-- --------------------------------------------------------

--
-- Table structure for table `ukuran`
--

CREATE TABLE `ukuran` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ukuran`
--

INSERT INTO `ukuran` (`id`, `nama`) VALUES
(6, 'Kecil'),
(7, 'besar'),
(8, 'Standard');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `alamat`, `agama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `username`, `password`, `level`) VALUES
(1, 'Roni Angga', 'Purbalingga', 'Islam', 'purbalingga', '2019-01-10', 'Laki-laki', 'roni', '123', 'user'),
(2, 'Admin', 'admin', 'Islam', 'admin', '2019-01-09', 'Laki-laki', 'admin', '123', 'admin'),
(6, 'Bagus', 'Purbalingga', 'islam', 'purbalingga', '2019-01-16', 'Perempuan', 'bagus123', '123', 'user'),
(7, 'Roni', 'Purbalingga', 'Islam', 'Purbalingga', '2019-01-15', 'Laki-laki', 'roni153', 'root', 'user'),
(9, 'Bintang', 'Purbalingga', 'Islam', 'Purbalingga', '2019-01-04', 'Laki-laki', 'bintang321', '321', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bangunan`
--
ALTER TABLE `bangunan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `letak`
--
ALTER TABLE `letak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merek`
--
ALTER TABLE `merek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ukuran`
--
ALTER TABLE `ukuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bangunan`
--
ALTER TABLE `bangunan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `letak`
--
ALTER TABLE `letak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `merek`
--
ALTER TABLE `merek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `mutasi`
--
ALTER TABLE `mutasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ukuran`
--
ALTER TABLE `ukuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
