<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
    }

    public function index()
    {
        if (isset($_POST['submit'])) {
            $username = $this->input->post('inputUsername');
            $password = $this->input->post('inputPassword');
            $hasil = $this->m_login->login($username, $password);
            if ($hasil->num_rows() == 1) {
                foreach ($hasil->result() as $sess) {
                    $sess_data['logged_in'] = 'Sudah Login';
                    $sess_data['username'] = $sess->username;
                    $sess_data['level'] = $sess->level;
                    $this->session->set_userdata($sess_data);
                }
                if ($this->session->userdata('level')=='tatausaha') {
                    redirect('tatausaha/dashboard');
                }
                elseif ($this->session->userdata('level')=='pegawai') {
                    redirect('/dashboard');
                }  
            } else {
                echo "<script>alert('Gagal login: Cek namaUser, password!');history.go(-1);</script>";
            }
        }
        else {
            $this->load->view('login');
        }
        
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
}

