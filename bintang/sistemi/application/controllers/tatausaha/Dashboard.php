<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
    }

    public function index()
    {
        $nama = $this->session->userdata('username');
        $data = array(
            "admin" => $this->admin->getdataadmin($nama)->result_array(),
            );
        $comp = array(
            "content" => $this->load->view("tatausaha/content_dashboard", $data, true),
            );
        $this->load->view('tatausaha/dashboard', $comp);
    }

}

?>

