<?php

class M_buku extends CI_model{

    public function getAllbuku(){
         return $this->db->get('buku')->result_array();
}

    public function tambah_data(){
        $data = [
            
            "judul" => $this->input->post('judul', true),
            "pengarang" => $this->input->post('pengarang', true),
            "penerbit" => $this->input->post('penerbit', true),
            "tahun_terbit" => $this->input->post('tahun_terbit', true)

        ];

        $this->db->insert('buku', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('buku', ['id' => $id]);
    }

    public function getAllbukubyid($id){

       return $this->db->get_where('buku',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "judul" => $this->input->post('judul', true),
            "pengarang" => $this->input->post('pengarang', true),
            "penerbit" => $this->input->post('penerbit', true),
            "tahun_terbit" => $this->input->post('tahun_terbit', true)

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('buku', $data);
    }

    

}
