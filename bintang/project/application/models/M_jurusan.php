<?php

class M_jurusan extends CI_model{

    public function getAlljurusan(){
         return $this->db->get('jurusan')->result_array();
}

    public function tambah_data(){
        $data = [
            "nama" => $this->input->post('nama', true)

        ];

        $this->db->insert('jurusan', $data);
    }

    public function hapus($id){

        // $this->db->where('id', $id);
        $this->db->delete('jurusan', ['id' => $id]);
    }

    public function getAlljurusanbyid($id){

       return $this->db->get_where('jurusan',['id' => $id])->row_array();
    }

     public function edit_data(){
        $data = [
            "nama" => $this->input->post('nama', true),

        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('jurusan', $data);
    }

    

}
?>