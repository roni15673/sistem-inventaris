<?php

class Barang extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_barang');
        $this->load->library('form_validation');

    }

    public function index(){

       
        $data['judul'] = 'Daftar Barang';
        $data['barang'] = $this->m_barang->getAllbarang();
        $this->load->view('templates/header', $data);
        $this->load->view('barang/v_barang');
        $this->load->view('templates/footer');
    }

    public function tambah(){
        
        $data['judul'] = 'Tambah data';
    
        $this->form_validation->set_rules('nama_barang', 'Nama_barang', 'required');
        $this->form_validation->set_rules('harga_satuan', 'harga_satuan', 'required');
        $this->form_validation->set_rules('jumlah_barang', 'Jumlah barang', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('barang/v_tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_barang->tambah_data();
            $this->session->set_flashdata('flash','Ditambahkan');
            redirect('barang');
        }
       
    }

    public function hapus($id){

        $this->m_barang->hapus($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('barang');

    }

    public function detail($id){

        $data['judul'] = 'detail';
        $data['barang'] = $this->m_barang->getAllbarangbyid($id);
        $this->load->view('templates/header', $data);
        $this->load->view('barang/detail', $data);
        $this->load->view('templates/footer');
    }
    

     public function edit($id){
        
        $data['judul'] = 'Edit data';
        $data['barang'] = $this->m_barang->getAllbarangbyid($id);

       $this->form_validation->set_rules('nama_barang', 'Nama_barang', 'required');
        $this->form_validation->set_rules('harga_satuan', 'harga_satuan', 'required');
        $this->form_validation->set_rules('jumlah_barang', 'Jumlah barang', 'required');
        if( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('barang/v_edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_barang->edit_data();
            $this->session->set_flashdata('flash','Diedit');
            redirect('barang');
        }
       
    }

}
?>