<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">
     <link rel="stylesheet" href="<?php echo base_url();?>asset/fontawesome/css/font-awesome.min.css" />
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">


    <title><?php echo $judul;?></title>
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">
  </head>
  <body>

  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?php echo base_url();?>index.php/home"><img src="<?php echo base_url();?>asset/img/logo.png" alt="..." width= 40px height=40px></td> Sistem Inventaris</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<div class="container ">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/home"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/barang"><i class="fa fa-cart-arrow-down"></i>

Daftar Barang</a></li>
         <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/buku"><i class="fa fa-bookmark "></i> Daftar Buku</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/user"><i class="fa fa-user   "></i> Daftar User</a></li>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
       <?php 
        if($this->session->userdata('permission') ==false ) { ?>
        <li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>/login"> Login</a></li> 
         <?php } ?>
         <?php 
        if($this->session->userdata('permission') ==true ) { ?>
        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('login/logout'); ?>">Logout</a></li>
         <?php } ?>
   

    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-success my-1 my-sm-0 " type="submit"><img src="<?php echo base_url();?>asset/img/search.png" alt="..." width= 20px height=20px margin-top:10px></button>
       <?php 
        if($this->session->userdata('permission') ==true ) { ?>
          <div class="akun">
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata("nama"); ?></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i>Akun</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><i class="fa fa-wrench "></i>Setting</a>
        </div>
      </div>
         <?php } ?>
           <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-align-justify"></i></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo site_url('sistem'); ?>"><i class="fa fa-user"></i>Sistem Inventaris</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><i class="fa fa-wrench "></i>link</a>
             <a class="dropdown-item" href="#"><i class="fa fa-wrench "></i>link</a>
                <a class="dropdown-item" href="#"><i class="fa fa-wrench "></i>link</a>
        </div>
      </li>
        </ul>
    </div>
    </form>
  </div>
</nav>

<!-- end navbar -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>