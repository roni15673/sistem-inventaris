<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
   
     <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/style.css">
     <link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logo.png">
    <title>Login</title>
  </head>
<body>
   <?php if($this->session->flashdata('flash') ) :?>
    <div class="row mt-3">
        <div class="col-md-6">
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
            Data<strong> berhasil!</strong> <?= $this->session->flashdata('flash'); ?> silahkan login
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
    </div>
<?php endif;?>

  <!-- login -->

  <div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
           
            <?php if(isset($error)) { echo $error; }; ?>
            <div class="account-wall">
                <center>
                  <h1 class="text-center login-title"><strong>Login</strong></h1>
                <img src="<?php echo base_url(); ?>asset/img/logo.png" width="80px" height="80px" alt="ini gambar ">
                </center>
                <form class="form-signin" method="POST" action="<?php echo site_url('login/aksi_login'); ?>">
                <div class="form-group">
                  <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <button class="btn btn-lg btn-primary btn-block" name="btn-login" id="btn-login" type="submit">Login</button>
               <p>Belum punya akun? <a href="<?php echo site_url();?>/user/tambah"><b>Daftar Sekarang!<b/></a></p>
            </div>
             <a href="<?php echo site_url(); ?>/home" class="btn btn-primary float-right"><i class="fa fa-arrow-circle-left  ">Home</i></a>
        </div>
    </div>
  </div>
  <script>  
    function runPopup(){
    window.alert("TULISKAN PESAN ANDA DI SINI");
    };
  </script>

									
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>